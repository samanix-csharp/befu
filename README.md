# BattlEye Filter Updater

Auto update BE filters locally or via FTP for Arma games.

## Current Supported Filters
* scripts.txt
* addweaponcargo.txt

Please feel free to send logs over to help improve and add new filters.

### Important Note

Some AV software thinks this is malware - the compiled downloads are not malware of any sort, Themida is used to protect a licensed component only.

#### Virus Total Reports
[**0.1.0**](https://www.virustotal.com/en/file/04d080df8db3ffc8c5a9da6660c7b7df5588dd0ace081df7a93b61e1ac6f201c/analysis/)

[**0.2.0**](https://www.virustotal.com/en/file/a0877dcb3c7495cf49c94113df3e0588f392a52664855b33560c7d6e562b73e7/analysis/)