﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Samanix.BEFU.Locale {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class License {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal License() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Samanix.BEFU.Locale.License", typeof(License).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GNU LESSER GENERAL PUBLIC LICENSE
        ///                       Version 3, 29 June 2007
        ///
        /// Copyright (C) 2007 Free Software Foundation, Inc. &lt;http://fsf.org/&gt;
        /// Everyone is permitted to copy and distribute verbatim copies
        /// of this license document, but changing it is not allowed.
        ///
        ///
        ///  This version of the GNU Lesser General Public License incorporates
        ///the terms and conditions of version 3 of the GNU General Public
        ///License, supplemented by the additional permissions listed below.
        ///
        ///  0. Additional Definitions.
        ///
        ///  As use [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string BattleNETLicense {
            get {
                return ResourceManager.GetString("BattleNETLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entypo (http://www.entypo.com/) is created by Daniel Bruce and released under the Creative Commons, Share Alike/Attribution license.
        ///
        ///http://creativecommons.org/licenses/by-sa/3.0/.
        /// </summary>
        internal static string EntypoLicense {
            get {
                return ResourceManager.GetString("EntypoLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The MIT License (MIT)
        ///
        ///Copyright (c) {0}
        ///
        ///Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the &quot;Software&quot;), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
        ///
        ///The above copyright notice and [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string MITLicense {
            get {
                return ResourceManager.GetString("MITLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Microsoft Public License.
        /// </summary>
        internal static string MsPL {
            get {
                return ResourceManager.GetString("MsPL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Microsoft Public License (Ms-PL)
        ///
        ///This license governs use of the accompanying software. If you use the software, you
        ///accept this license. If you do not accept the license, do not use the software.
        ///
        ///1. Definitions
        ///The terms &quot;reproduce,&quot; &quot;reproduction,&quot; &quot;derivative works,&quot; and &quot;distribution&quot; have the
        ///same meaning here as under U.S. copyright law.
        ///A &quot;contribution&quot; is the original software, or any additions or changes to the software.
        ///A &quot;contributor&quot; is any person that distributes its contribution und [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string NinjectLicense {
            get {
                return ResourceManager.GetString("NinjectLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to # License
        ///
        ///Please carefully understand the license and download the latest icons at ModernUIIcons.com.
        ///
        ///## Understand Your Rights
        ///No Attribution and No Derived Works
        ///http://creativecommons.org/licenses/by-nd/3.0/ *
        ///
        ///- If your project is open source include this license file in the source.
        ///- Nothing is needed in the front facing project (UNLESS you
        ///  are using any of the icons listed below in the attribution section).
        ///- Commercial use is not only allowed but encouraged. If it is an icon
        ///  in the  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string WindowsIconsLicense {
            get {
                return ResourceManager.GetString("WindowsIconsLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} License.
        /// </summary>
        internal static string WindowTitleLicense {
            get {
                return ResourceManager.GetString("WindowTitleLicense", resourceCulture);
            }
        }
    }
}
