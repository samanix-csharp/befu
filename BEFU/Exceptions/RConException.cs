﻿using System;

namespace Samanix.BEFU.Exceptions
{
    public class RConException : Exception
    {
        public RConException()
        {
        }

        public RConException(string message) : base(message)
        {
        }

        public RConException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}