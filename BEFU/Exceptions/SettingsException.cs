﻿using System;

namespace Samanix.BEFU.Exceptions
{
    internal class SettingsException : Exception
    {
        public SettingsException()
        {

        }

        public SettingsException(string message) : base(message)
        {

        }

        public SettingsException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
