﻿using System;

namespace Samanix.BEFU.Exceptions
{
    public class NoEnabledServersException : Exception
    {
        public NoEnabledServersException()
        {
        }

        public NoEnabledServersException(string message) : base(message)
        {
        }

        public NoEnabledServersException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class NoBeFilesLoadedException : Exception
    {
        public NoBeFilesLoadedException()
        {
        }

        public NoBeFilesLoadedException(string message) : base(message)
        {
        }

        public NoBeFilesLoadedException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}