using Ninject;
using Samanix.BEFU.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Caliburn.Micro;
using Samanix.BEFU.Models;
using Samanix.BEFU.Models.IO;
using Samanix.BEFU.ViewModels.Flyouts;

namespace Samanix.BEFU
{
    // TODO Update checker - currently just opens befu homepages

    public class AppBootstrapper : BootstrapperBase
    {
        private IKernel kernel;
        
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            kernel = new StandardKernel();

            kernel.Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
            kernel.Bind<IEventAggregator>().To<EventAggregator>().InSingletonScope();

            // Models
            kernel.Bind<LogFileManager>().ToSelf().InSingletonScope();
            kernel.Bind<Config>().ToSelf().InSingletonScope();
            kernel.Bind<BefuVersion>().ToSelf().InSingletonScope();
            kernel.Bind<IFilterManager>().To<RemoteFilterManager>().Named("RemoteFilterManager");
            kernel.Bind<IFilterManager>().To<LocalFilterManager>().Named("LocalFilterManager");
            kernel.Bind<FtpTester>().ToSelf().InSingletonScope();
            kernel.Bind<FilterUpdater>().ToSelf().InSingletonScope();

            // View models
            kernel.Bind<ViewModelBase>().To<MainWindowViewModel>().InSingletonScope().Named("MainWindowVM");
            
            kernel.Bind<ViewModelBase>().To<AboutViewModel>().InSingletonScope().Named("AboutVM");
            kernel.Bind<ViewModelBase>().To<LicenseViewModel>().InSingletonScope().Named("LicenseVM");

            kernel.Bind<FlyoutViewModelBase>().To<SettingsFlyoutViewModel>().InSingletonScope().Named("SettingsFlyoutVM");
        }

        protected override object GetInstance(Type service, string key)
        {
            return kernel.Get(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return kernel.GetAll(service);
        }

        protected override void BuildUp(object instance)
        {
            kernel.Inject(instance);
        }
        
        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            try
            {
                Logger.LogLevel =
                    (LogLevel) Enum.Parse(typeof(LogLevel), Properties.Settings.Default.LogLevel ?? "Info");
            }
            catch
            {
                Logger.LogLevel = LogLevel.Info;
            }

            Config config = kernel.Get<Config>();

            if (!Directory.Exists(Config.LogDirectory))
            {
                Directory.CreateDirectory(Config.LogDirectory);
            }

            if (!Directory.Exists(Config.FilterDirectory))
            {
                DirectoryInfo di = Directory.CreateDirectory(Config.FilterDirectory);
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }

            kernel.Get<LogFileManager>().StartLogFileManager();
            
            //Limilabs.FTP.Log.Enabled = true;
            //Limilabs.FTP.Log.WriteLine += Console.WriteLine;

            config.Load();

            kernel.Get<IWindowManager>().ShowWindow(kernel.Get<ViewModelBase>("MainWindowVM"));
        }

        protected override void OnExit(object sender, EventArgs e)
        {
            Logger.Info(Locale.Phrases.BefuShutdown);

            kernel.Get<ViewModelBase>("AboutVM").TryClose();
            kernel.Get<ViewModelBase>("MainWindowVM").Dispose();
            kernel.Get<FlyoutViewModelBase>("SettingsFlyoutVM").Dispose();

            kernel.Get<IFilterManager>("RemoteFilterManager").Dispose();
            kernel.Get<FilterUpdater>().Dispose();
            kernel.Get<LogFileManager>().Dispose();

            kernel.Dispose();
            base.OnExit(sender, e);
        }
        
    }
}