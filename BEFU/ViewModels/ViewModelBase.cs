﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Caliburn.Micro;

namespace Samanix.BEFU.ViewModels
{
    public abstract class ViewModelBase : Screen, IAppViewModel, INotifyDataErrorInfo
    {
        private readonly IDictionary<string, IDictionary<string, string>> validationErrors = new Dictionary<string, IDictionary<string, string>>();
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public bool IsValid => !HasErrors;
        public bool HasErrors => validationErrors.Count > 0;

        private bool _isOpen = false;
        /// <summary>
        /// Check if view is open
        /// </summary>
        public bool IsOpen
        {
            get
            {
                return _isOpen;
                
            }
            set
            {
                if(value.Equals(_isOpen))
                {
                    return;
                }
                _isOpen = value;
                NotifyOfPropertyChange(() => IsOpen);
            }
        }

        /// <summary>
        /// Check if disposing underway
        /// </summary>
        public bool IsDisposing { get; protected set; } = false;

        /// <summary>
        /// Check if object disposed
        /// </summary>
        public bool IsDisposed { get; protected set; } = false;

        /// <summary>
        /// Abstract method
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// Get Errors
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public IEnumerable GetErrors(string propertyName)
        {
            if (propertyName == null || !validationErrors.ContainsKey(propertyName) || validationErrors[propertyName].Count == 0)
            {
                return null;
            }

            return validationErrors[propertyName].Values.ToList();
        }

        /// <summary>
        /// Errors changed
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnErrorsChanged(string propertyName)
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }
        
        /// <summary>
        /// Add/Remove error
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="errorKey"></param>
        /// <param name="valueValid">If value is valid, remove existing errors for the key</param>
        /// <param name="errorMessage"></param>
        /// <param name="parts"></param>
        public bool ValueValid(string propertyName, string errorKey, bool valueValid, string errorMessage, params object[] parts)
        {
            if (valueValid)
            {
                RemoveValidationError(propertyName, errorKey, string.Format(errorMessage, parts));
            }
            else
            {
                AddValidationError(propertyName, errorKey, string.Format(errorMessage, parts));
            }

            return valueValid;
        }
        
        /// <summary>
        /// Add errors
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="errorKey"></param>
        /// <param name="error"></param>
        public void AddValidationError(string propertyName, string errorKey, string error)
        {
            if (!validationErrors.ContainsKey(propertyName))
            {
                validationErrors.Add(propertyName, new Dictionary<string, string>());
            }

            if (validationErrors[propertyName].ContainsKey(errorKey))
            {
                return;
            }
            
            validationErrors[propertyName].Add(errorKey, error);
            OnErrorsChanged(propertyName);
        }

        /// <summary>
        /// Add multiple errors
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="errors"></param>
        public void AddValidationError(string propertyName, IDictionary<string, string> errors)
        {
            if (!validationErrors.ContainsKey(propertyName))
            {
                validationErrors.Add(propertyName, new Dictionary<string, string>());
            }

            foreach (KeyValuePair<string, string> error in errors)
            {
                if (validationErrors[propertyName].ContainsKey(error.Key))
                {
                    continue;
                }

                validationErrors[propertyName].Add(error.Key, error.Value);
            }

            OnErrorsChanged(propertyName);
        }

        /// <summary>
        /// Remove error
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="errorKey"></param>
        /// <param name="error"></param>
        public void RemoveValidationError(string propertyName, string errorKey, string error)
        {
            if (!validationErrors.ContainsKey(propertyName) ||
                !validationErrors[propertyName].ContainsKey(errorKey))
            {
                return;
            }

            validationErrors[propertyName].Remove(errorKey);

            if (validationErrors[propertyName].Count == 0)
            {
                validationErrors.Remove(propertyName);
            }

            OnErrorsChanged(propertyName);
        }

        /// <summary>
        /// Remove multiple errors
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="errors"></param>
        public void RemoveValidationError(string propertyName, IDictionary<string, string> errors)
        {
            if (!validationErrors.ContainsKey(propertyName))
            {
                return;
            }

            foreach (KeyValuePair<string, string> error in errors)
            {
                if (validationErrors[propertyName].ContainsKey(error.Key))
                {
                    continue;
                }

                validationErrors[propertyName].Remove(error.Key);
            }

            OnErrorsChanged(propertyName);
        }

        /// <summary>
        /// Remove all errors from given property
        /// </summary>
        /// <param name="propertyName"></param>
        public void RemoveAllErrors(string propertyName)
        {
            if (!validationErrors.ContainsKey(propertyName))
            {
                return;
            }

            validationErrors.Remove(propertyName);
            OnErrorsChanged(propertyName);
        }

        /// <summary>
        /// Reset errors to nil
        /// </summary>
        public void ResetErrors()
        {
            validationErrors.Clear();
        }
    }
}