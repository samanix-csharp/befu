using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Samanix.BEFU.Models;
using Samanix.BEFU.Events;
using Samanix.BEFU.Models.IO;
using Samanix.BEFU.ViewModels.Flyouts;
using Samanix.BEFU.Views;

namespace Samanix.BEFU.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IDisposable,
        IHandle<ServerNameUpdateEvent>, IHandle<ServerAddEvent>, IHandle<LogEvent>, IHandle<BefuRunComplete>
    {
        /// <summary>
        /// BEFU config
        /// </summary>
        private readonly Config Config;

        /// <summary>
        /// Main window view
        /// </summary>
        private MainWindowView mainWindowView;
        

        private string _befuWindowTitle;
        /// <summary>
        /// Window title
        /// </summary>
        public string BefuWindowTitle
        {
            get { return _befuWindowTitle; }
            set
            {
                if (value.Equals(_befuWindowTitle))
                {
                    return;
                }

                _befuWindowTitle = value;
                NotifyOfPropertyChange(() => BefuWindowTitle);
            }
        }

        public MainWindowViewModel(BefuVersion version, Config config, IEventAggregator eventAggregator)
        {
            Config = config;
            Version = version.VersionText;
            eventAggregator.Subscribe(this);

            ServerList = Config.Settings.Servers;
            ServerDataGridCollection = new BindableCollection<string>(ServerList.Keys);

            SetButtonStates();

            LogOutputWindow = new BindableCollection<string>();

            InitFlyouts();

            BefuWindowTitle = Locale.Befu.BefuFull;

            Logger.Info($"{Locale.Befu.BefuShort} {Version}");
#if DEBUG
            Logger.Info(Locale.Phrases.DebugMode);
#endif
        }

        #region Dispose
        ~MainWindowViewModel()
        {
            Dispose(false);
        }

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of timer
        /// </summary>
        private void Dispose(bool freeManaged)
        {
            if (!freeManaged || IsDisposing || IsDisposed)
            {
                return;
            }

            IsDisposing = true;

            Logger.Trace(Locale.Phrases.DisposingManagedResourcesFor, GetType());

            _runButtonTimer?.Stop();
            _runButtonTimer?.Dispose();

            IsDisposed = true;
            IsDisposing = false;
        }
        #endregion

        /// <summary>
        /// Handle a link being clicked
        /// </summary>
        /// <param name="sender"></param>
        public void LinkButtonClick(object sender)
        {
            Button button = sender as Button;

            if (button == null)
            {
                Logger.Trace(Locale.Phrases.NullButtonPressed);
                return;
            }

            string link;

            switch (button.Name)
            {
                case "BefuHomePage":
                case "BefuUpdateCheck":
                    link = "https://www.samanix.com/befu";
                    break;
                case "BefuDocsLink":
                    link = "https://www.samanix.com/befu/docs";
                    break;
                case "BefuReportIssue":
                    link = "https://www.samanix.com/befu/issue";
                    break;
                case "BefuRequestFeature":
                    link = "https://www.samanix.com/befu/requestfeature";
                    break;
                case "SamanixLinkButton":
                    link = "https://www.samanix.com";
                    break;
                case "GamingDeluxeLinkButton":
                    link = "https://www.gamingdeluxe.co.uk";
                    break;
                default:
                    return;
            }

            if (string.IsNullOrEmpty(link))
            {
                return;
            }

            Process.Start(link);
        }

        /// <summary>
        /// Get view
        /// </summary>
        /// <returns></returns>
        private MainWindowView GetView()
        {
            return mainWindowView ?? (mainWindowView = base.GetView() as MainWindowView);
        }

        /// <summary>
        /// Open about window
        /// </summary>
        public void ToggleAboutWindow()
        {
            ViewModelBase aboutVm = IoC.Get<ViewModelBase>("AboutVM");

            if (aboutVm == null)
            {
                return;
            }

            if (aboutVm.IsOpen)
            {
                aboutVm.TryClose();
                return;
            }

            IoC.Get<IWindowManager>().ShowWindow(IoC.Get<ViewModelBase>("AboutVM"));
        }
        
        /// <summary>
        /// Initialise flyouts
        /// </summary>
        private void InitFlyouts()
        {
            FlyoutViewModels.Add("settings", IoC.Get<SettingsFlyoutViewModel>());
        }
        
        private string _version;
        public string Version
        {
            get { return _version; }
            set
            {
                _version = value;
                NotifyOfPropertyChange(() => Version);
            }
        }
        
        private IDictionary<string, ServerInfo> _serverList;
        public IDictionary<string, ServerInfo> ServerList
        {
            get { return _serverList; }
            set
            {
                _serverList = value;
                NotifyOfPropertyChange(() => ServerList);
            }
        }

        private IObservableCollection<string> _serverDataGridCollection;
        public IObservableCollection<string> ServerDataGridCollection
        {
            get { return _serverDataGridCollection; }
            set
            {
                _serverDataGridCollection = value;
                
                NotifyOfPropertyChange(() => ServerDataGridCollection);
            }
        }

        private int _selectedIndex = -1;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (value.Equals(_selectedIndex))
                {
                    return;
                }

                _selectedIndex = value;
                
                NotifyOfPropertyChange(() => SelectedIndex);
            }
        }
        
        private IObservableCollection<string> _logOutputWindow;
        public IObservableCollection<string> LogOutputWindow
        {
            get { return _logOutputWindow; }
            set
            {
                _logOutputWindow = value;
                NotifyOfPropertyChange(() => LogOutputWindow);
            }
        }

        public IDictionary<string, FlyoutViewModelBase> FlyoutViewModels { get; } = new Dictionary<string, FlyoutViewModelBase>();
        
        private string currentSettingsFlyoutActive;

        private bool befuRunning;

        private bool _addNewServerEnabled = true;
        public bool AddNewServerEnabled
        {
            get { return _addNewServerEnabled; }
            set
            {
                if (value.Equals(_addNewServerEnabled))
                {
                    return;
                }

                _addNewServerEnabled = value;
                NotifyOfPropertyChange(() => AddNewServerEnabled);
            }
        }

        private bool _deleteServerEnabled = true;
        public bool DeleteServerEnabled
        {
            get { return _deleteServerEnabled; }
            set
            {
                if (value.Equals(_deleteServerEnabled))
                {
                    return;
                }

                _deleteServerEnabled = value;
                NotifyOfPropertyChange(() => DeleteServerEnabled);
            }
        }

        private string _befuRunButtonText = Locale.Phrases.Run;
        public string BefuRunButtonText
        {
            get { return _befuRunButtonText; }
            set
            {
                if (value.Equals(_befuRunButtonText))
                {
                    return;
                }

                _befuRunButtonText = value;
                NotifyOfPropertyChange(() => BefuRunButtonText);
            }
        }

        private bool _befuRunButtonEnabled = true;
        public bool BefuRunButtonEnabled
        {
            get { return _befuRunButtonEnabled; }
            set
            {
                if (value.Equals(_befuRunButtonEnabled))
                {
                    return;
                }
                _befuRunButtonEnabled = value;
                NotifyOfPropertyChange(() => BefuRunButtonEnabled);
            }
        }
        
        /// <summary>
        /// Get flyout from dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public FlyoutViewModelBase GetFlyout(string key)
        {
            return !FlyoutViewModels.ContainsKey(key) ? null : FlyoutViewModels[key];
        }
        
        /// <summary>
        /// Open settings flyout on double click
        /// </summary>
        public void ServerListServerDoubleClick()
        {
            if (befuRunning)
            {
                BefuRunningMessage();
                return;
            }

            string serverId = GetSelectedServer();
            
            ServerInfo server = Config.GetServerSettings(serverId);
            
            if (server == null)
            {
                return;
            }

            SettingsFlyoutViewModel flyout = GetFlyout("settings") as SettingsFlyoutViewModel;

            if (flyout == null)
            {
                return;
            }
            
            if (currentSettingsFlyoutActive == serverId && flyout.IsOpen)
            {
                flyout.CloseFlyout();
                return;
            }

            currentSettingsFlyoutActive = serverId;
            
            flyout.OpenFlyout(serverId, server);
        }

        /// <summary>
        /// Add new server
        /// </summary>
        public void AddNewServer()
        {
            if (befuRunning)
            {
                BefuRunningMessage();
                return;
            }

            SettingsFlyoutViewModel flyout = GetFlyout("settings") as SettingsFlyoutViewModel;

            if (flyout == null)
            {
                return;
            }

            flyout.CloseFlyout();
            
            flyout.OpenFlyout("new-server", null, true);
        }

        /// <summary>
        /// Delete server
        /// </summary>
        public void DeleteServer()
        {
            if (befuRunning)
            {
                BefuRunningMessage();
                return;
            }

            string serverId = GetSelectedServer();
            
            MessageBoxResult confirm = MessageBox.Show(Locale.Phrases.ConfirmServerDeleteMessage, Locale.Phrases.ConfirmServerDelete, MessageBoxButton.YesNo);

            if (confirm == MessageBoxResult.No)
            {
                return;
            }

            ServerDataGridCollection.Remove(serverId);
            
            Config.DeleteServer(serverId);

            SettingsFlyoutViewModel flyout = GetFlyout("settings") as SettingsFlyoutViewModel;

            flyout?.CloseFlyout();

            ServerDataGridCollection.Refresh();

            SetButtonStates();
        }

        /// <summary>
        /// Get text value of server list item
        /// </summary>
        /// <returns></returns>
        private string GetSelectedServer()
        {
            return ServerDataGridCollection.ElementAt(SelectedIndex);
        }

        /// <summary>
        /// Update server name
        /// </summary>
        /// <param name="server"></param>
        public void Handle(ServerNameUpdateEvent server)
        {
            int index = ServerDataGridCollection.IndexOf(server.OldServerName);

            ServerDataGridCollection[index] = server.NewServerName;

            ServerDataGridCollection.Refresh();
        }

        /// <summary>
        /// Update server name
        /// </summary>
        /// <param name="server"></param>
        public void Handle(ServerAddEvent server)
        {
            ServerDataGridCollection.Add(server.NewServerName);

            SelectedIndex = ServerDataGridCollection.Count - 1;

            SetButtonStates();
        }
        
        /// <summary>
        /// Add log message to output window
        /// </summary>
        /// <param name="log"></param>
        public void Handle(LogEvent log)
        {
            if (LogOutputWindow.Count > 100)
            {
                LogOutputWindow.RemoveAt(0);
            }

            LogOutputWindow.Add(log.Message);

            GetView()?.LogScrollViewer.ScrollToBottom();
        }

        /// <summary>
        /// Run BEFU
        /// </summary>
        public void RunBefu()
        {
            if (befuRunning)
            {
                // TODO already running, cancel
                return;
            }
            
            befuRunning = true;

            BefuWindowTitle = $"{Locale.Befu.BefuShort} {Locale.Phrases.Running}";
            
            BefuRunButtonText = Locale.Phrases.Running;

            FlyoutViewModelBase flyout = GetFlyout("settings");

            flyout?.CloseFlyout();

            IoC.Get<FilterUpdater>().Run();
        }
        
        /// <summary>
        /// Befu already running message
        /// </summary>
        private static void BefuRunningMessage()
        {
            MessageBox.Show(Locale.Phrases.BefuRunningCurrently, Locale.Phrases.Error);
        }

        /// <summary>
        /// Fired when befu has finished running
        /// </summary>
        /// <param name="befuRun"></param>
        public void Handle(BefuRunComplete befuRun)
        {
            befuRunning = false;

            BefuWindowTitle = befuRun.Success ? Locale.Befu.BefuRunSuccessTitle : Locale.Befu.BefuRunFailedTitle;
            BefuRunButtonText = Locale.Phrases.Run;

            _runButtonTimer = new Timer(2000);
            _runButtonTimer.Elapsed += RunButtonTimerElapsed;
            _runButtonTimer.AutoReset = false;
            _runButtonTimer.Enabled = true;

            _runButtonTimer.Start();

            Logger.Info(befuRun.Success ? Locale.Befu.BefuRunSuccess : Locale.Befu.BefuRunError);
        }

        private Timer _runButtonTimer;
        /// <summary>
        /// _saveTimer event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void RunButtonTimerElapsed(object source, ElapsedEventArgs e)
        {
            _runButtonTimer?.Stop();
            _runButtonTimer?.Dispose();

            SetButtonStates();

            BefuWindowTitle = Locale.Befu.BefuFull;
        }

        /// <summary>
        /// Set button state
        /// </summary>
        private void SetButtonStates()
        {
            BefuRunButtonEnabled = ServerDataGridCollection.Count > 0 && !befuRunning;
            DeleteServerEnabled = SelectedIndex != -1 && !befuRunning;
            AddNewServerEnabled = !befuRunning;
        }
    }
}