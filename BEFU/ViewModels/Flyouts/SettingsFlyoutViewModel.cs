﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Media;
using Caliburn.Micro;
using MahApps.Metro.Controls;
using Samanix.BEFU.Models;
using Samanix.BEFU.Events;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.ViewModels.Flyouts
{
    public class SettingsFlyoutViewModel : FlyoutViewModelBase, IDisposable, 
        IHandle<FtpTestCompleteEvent>, IHandle<ServerNameUpdateEvent>, IHandle<ServerSaveEvent>
    {
        private readonly Config config;
        private readonly IEventAggregator events;
        
        public SettingsFlyoutViewModel(Config cfg, IEventAggregator eventAggregator)
        {
            config = cfg;
            events = eventAggregator;
            events.Subscribe(this);
            
            Position = Position.Right;
            Header = Locale.Phrases.Settings;
            
            FtpEncryptionModeDropDown = Enum.GetValues(typeof(FtpEncryptionMode));
        }
        
        private string selectedServerId = null;

        private bool addingServer = false;

        ~SettingsFlyoutViewModel()
        {
            Dispose(false);
        }

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of timer
        /// </summary>
        private void Dispose(bool freeManaged)
        {
            if (!freeManaged || IsDisposing  || IsDisposed)
            {
                return;
            }

            IsDisposing = true;

            Logger.Trace(Locale.Phrases.DisposingManagedResourcesFor, GetType());

            _saveTimer?.Stop();
            _saveTimer?.Dispose();

            _ftpTimer?.Stop();
            _ftpTimer?.Dispose();

            IsDisposed = true;
            IsDisposing = false;
        }

        #region Form
        private string _serverName;
        public string ServerName
        {
            get { return _serverName; }
            set
            {
                if (value.Equals(_serverName))
                {
                    return;
                }
                _serverName = value;
                ServerNameValid();
                NotifyOfPropertyChange(() => ServerName);
            }
        }

        private bool ServerNameValid()
        {
            bool isNotEmpty = ValueValid("ServerName", "is-empty", !(string.IsNullOrEmpty(ServerName) || ServerName == ""), Locale.Validation.CannotBeEmpty);

            bool validChars = ValueValid("ServerName", "valid-chars", ServerName.IndexOfAny(Path.GetInvalidFileNameChars()) == -1, Locale.Validation.InvalidCharacters);

            bool notExists = ServerName != selectedServerId ? 
                ValueValid("ServerName", "exists", config.Settings.Servers.All(server => server.Key != ServerName), Locale.Validation.ValueExists) : 
                ValueValid("ServerName", "exists", true, string.Empty);

            return isNotEmpty && validChars && notExists;
        }

        private bool _serverEnabled = true;
        public bool ServerEnabled
        { 
            get { return _serverEnabled; }
            set
            {
                if (value.Equals(_serverEnabled))
                {
                    return;
                }
                _serverEnabled = value;
                NotifyOfPropertyChange(() => ServerEnabled);
            }
        }

        private bool _rconReloadEnabled = true;
        public bool RConReloadEnabled
        {
            get { return _rconReloadEnabled; }
            set
            {
                if (value.Equals(_rconReloadEnabled))
                {
                    return;
                }
                _rconReloadEnabled = value;
                NotifyOfPropertyChange(() => RConReloadEnabled);
            }
        }

        private bool _ftpMode = true;
        public bool FtpMode
        {
            get { return _ftpMode; }
            set
            {
                if (value.Equals(_ftpMode))
                {
                    return;
                }
                TestButtonEnabled = value;
                _ftpMode = value;
                NotifyOfPropertyChange(() => FtpMode);
            }
        }
        
        private bool _directoryMode = false;
        public bool DirectoryMode
        {
            get { return _directoryMode; }
            set
            {
                if (value.Equals(_directoryMode))
                {
                    return;
                }

                _directoryMode = value;
                NotifyOfPropertyChange(() => DirectoryMode);
            }
        }
        
        private Array _ftpEncryptionModeDropDown;
        public Array FtpEncryptionModeDropDown
        {
            get { return _ftpEncryptionModeDropDown; }
            set
            {
                _ftpEncryptionModeDropDown = value;
                NotifyOfPropertyChange(() => FtpEncryptionModeDropDown);
            }
        }

        private int _ftpEncryptionModeSelected = 0;
        public int FtpEncryptionModeSelected
        {
            get { return _ftpEncryptionModeSelected; }
            set
            {
                _ftpEncryptionModeSelected = value;
                NotifyOfPropertyChange(() => FtpEncryptionModeSelected);
            }
        }

        private bool _ftpPassiveEnabled = true;
        public bool FtpPassiveEnabled
        {
            get { return _ftpPassiveEnabled; }
            set
            {
                if (value.Equals(_ftpPassiveEnabled))
                {
                    return;
                }
                _ftpPassiveEnabled = value;
                NotifyOfPropertyChange(() => FtpPassiveEnabled);
            }
        }

        private string _ftpIp;
        public string FtpIp
        {
            get { return _ftpIp; }
            set
            {
                if (value.Equals(_ftpIp))
                {
                    return;
                }

                _ftpIp = value;
                FtpIpValid();
                NotifyOfPropertyChange(() => FtpIp);
            }
        }

        private bool FtpIpValid()
        {
            return !FtpMode || ValueValid("FtpIp", "not-empty", !string.IsNullOrEmpty(FtpIp), Locale.Validation.CannotBeEmpty);
        }

        private string _ftpPort;
        public string FtpPort
        {
            get { return _ftpPort; }
            set
            {
                if (value.Equals(_ftpPort))
                {
                    return;
                }

                _ftpPort = value;
                FtpPortValid();
                NotifyOfPropertyChange(() => FtpPort);
            }
        }

        private bool FtpPortValid()
        {
            if (!FtpMode)
            {
                return true;
            }

            int port;
            bool result = int.TryParse(FtpPort, out port);
            
            return ValueValid("FtpPort", "valid-port", result, Locale.Validation.InvalidInt) 
                && ValueValid("FtpPort", "valid-range", (port >= 1 && port <= 65535), Locale.Validation.InvalidIntRange, 1, 65535);
        }

        private string _ftpUser;
        public string FtpUser
        {
            get { return _ftpUser; }
            set
            {
                if (value.Equals(_ftpUser))
                {
                    return;
                }

                _ftpUser = value;
                FtpUserValid();
                NotifyOfPropertyChange(() => FtpUser);
            }
        }

        private bool FtpUserValid()
        {
            return !FtpMode || ValueValid("FtpUser", "not-empty", !string.IsNullOrEmpty(FtpUser), Locale.Validation.CannotBeEmpty);
        }

        private string _ftpPass;
        public string FtpPass
        {
            get { return _ftpPass; }
            set
            {
                if (value.Equals(_ftpPass))
                {
                    return;
                }

                _ftpPass = value;
                FtpPassValid();
                NotifyOfPropertyChange(() => FtpPass);
            }
        }

        private bool FtpPassValid()
        {
            return !FtpMode || ValueValid("FtpPass", "not-empty", !string.IsNullOrEmpty(FtpPass), Locale.Validation.CannotBeEmpty);
        }

        private string _ftpBePath;
        public string FtpBePath
        {
            get { return _ftpBePath; }
            set
            {
                if (value.Equals(_ftpBePath))
                {
                    return;
                }

                _ftpBePath = value;
                FtpBePathValid();
                NotifyOfPropertyChange(() => FtpBePath);
            }
        }

        private bool FtpBePathValid()
        {
            return !FtpMode || ValueValid("FtpBePath", "not-empty", !string.IsNullOrEmpty(FtpBePath), Locale.Validation.CannotBeEmpty);
        }

        private string _directoryBePath;
        public string DirectoryBePath
        {
            get { return _directoryBePath; }
            set
            {
                if (value.Equals(_directoryBePath))
                {
                    return;
                }

                _directoryBePath = value;
                DirectoryBePathValid();
                NotifyOfPropertyChange(() => DirectoryBePath);
            }
        }

        private bool DirectoryBePathValid()
        {
            if (!DirectoryMode)
            {
                return true;
            }

            if (!ValueValid("DirectoryBePath", "not-empty", !string.IsNullOrEmpty(DirectoryBePath), Locale.Validation.CannotBeEmpty))
            {
                return false;
            }
            
            if (!ValueValid("DirectoryBePath", "dir-exists", Directory.Exists(DirectoryBePath), Locale.Validation.DirectoryNotExist))
            {
                return false;
            }
            
            if (!ValueValid("DirectoryBePath", "beserver-exists", File.Exists(Path.Combine(DirectoryBePath, "beserver.dll")), Locale.Validation.BeServerDllNotFound))
            {
                return false;
            }

            string[] files = Directory.GetFiles(DirectoryBePath, "beserver*.cfg", SearchOption.TopDirectoryOnly);

            return ValueValid("DirectoryBePath", "beservercfg-exists", files.Length > 0, Locale.Validation.BeServerConfigNotFound);
            
        }
        #endregion

        #region Form Buttons/Actions
        private bool _saveButtonEnabled = true;
        public bool SaveButtonEnabled
        {
            get { return _saveButtonEnabled; }
            set
            {
                if (value.Equals(_saveButtonEnabled))
                {
                    return;
                }

                _saveButtonEnabled = value;
                NotifyOfPropertyChange(() => SaveButtonEnabled);
            }
        }

        private string _saveButtonText = Locale.Phrases.Save;
        public string SaveButtonText
        {
            get { return _saveButtonText; }
            set
            {
                if (value.Equals(_saveButtonText))
                {
                    return;
                }
                _saveButtonText = value;
                NotifyOfPropertyChange(() => SaveButtonText);
            }
        }

        
        private bool _testButtonEnabled = true;
        public bool TestButtonEnabled
        {
            get { return _testButtonEnabled; }
            set
            {
                if (value.Equals(_testButtonEnabled))
                {
                    return;
                }

                _testButtonEnabled = value;
                NotifyOfPropertyChange(() => TestButtonEnabled);
            }
        }

        private string _testButtonText = Locale.Phrases.Test;
        public string TestButtonText
        {
            get { return _testButtonText; }
            set
            {
                if (value.Equals(_testButtonText))
                {
                    return;
                }
                _testButtonText = value;
                NotifyOfPropertyChange(() => TestButtonText);
            }
        }

        /// <summary>
        /// Set values in settings flyout
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="settings"></param>
        /// <param name="addingNew"></param>
        private void SetValues(string serverId, ServerInfo settings = null, bool addingNew = false)
        {
            addingServer = addingNew;

            if (addingServer || settings == null)
            {
                selectedServerId = null;
                ServerName = string.Empty;

                FtpMode = true;
                DirectoryMode = false;
                ServerEnabled = true;
                RConReloadEnabled = true;
                
                FtpIp = string.Empty;
                FtpPort = string.Empty;
                FtpUser = string.Empty;
                FtpPass = string.Empty;
                FtpBePath = string.Empty;
                FtpEncryptionModeSelected = 0;
                FtpPassiveEnabled = true;

                DirectoryBePath = string.Empty;

                TestButtonEnabled = true;

                return;
            }

            try
            {
                selectedServerId = serverId;
                ServerName = serverId;
                ServerEnabled = settings.ServerEnabled;
                RConReloadEnabled = settings.RConReloadEnabled;

                if (settings.FtpEnabled)
                {
                    FtpMode = true;
                    DirectoryMode = false;

                    FtpIp = settings.FtpInfo.Ip;
                    FtpPort = settings.FtpInfo.Port.ToString();
                    FtpUser = settings.FtpInfo.User;
                    FtpPass = settings.FtpInfo.Pass;
                    FtpBePath = settings.FtpInfo.Path;
                    FtpEncryptionModeSelected = (int)settings.FtpInfo.FtpEncryptionMode;
                    FtpPassiveEnabled = settings.FtpInfo.Passive;

                    DirectoryBePath = string.Empty;
                }
                else
                {
                    FtpMode = false;
                    DirectoryMode = true;

                    DirectoryBePath = settings.Directory;

                    FtpIp = string.Empty;
                    FtpPort = string.Empty;
                    FtpUser = string.Empty;
                    FtpPass = string.Empty;
                    FtpBePath = string.Empty;
                    FtpEncryptionModeSelected = 0;
                    FtpPassiveEnabled = true;
                }

                TestButtonEnabled = FtpMode;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, Locale.Phrases.Error);
            }
        }

        private Visibility _ftpTestMessageVisibility = Visibility.Hidden;
        public Visibility FtpTestMessageVisibility
        {
            get { return _ftpTestMessageVisibility; }
            set
            {
                if (value.Equals(_ftpTestMessageVisibility))
                {
                    return;
                }

                _ftpTestMessageVisibility = value;
                NotifyOfPropertyChange(() => FtpTestMessageVisibility);
            }
        }

        private string _ftpTestMessage;
        public string FtpTestMessage
        {
            get { return _ftpTestMessage; }
            set
            {
                if (value.Equals(_ftpTestMessage))
                {
                    return;
                }

                _ftpTestMessage = value;
                NotifyOfPropertyChange(() => FtpTestMessage);
            }
        }

        private Brush _ftpTestMessageColour = Brushes.White;
        public Brush FtpTestMessageColour
        {
            get { return _ftpTestMessageColour; }
            set
            {
                if (value.Equals(_ftpTestMessageColour))
                {
                    return;
                }

                _ftpTestMessageColour = value;
                NotifyOfPropertyChange(() => FtpTestMessageColour);
            }
        }

        private readonly IDictionary<string, FtpTestInfo> ftpTests = new Dictionary<string, FtpTestInfo>();
        #endregion

        /// <summary>
        /// Open flyout
        /// </summary>
        public override void OpenFlyout()
        {
            if (selectedServerId != null && ftpTests.ContainsKey(selectedServerId))
            {
                FtpTestMessageVisibility = Visibility.Visible;

                switch (ftpTests[selectedServerId].Status)
                {
                    case FtpTestStatus.Running:
                        TestButtonEnabled = false;
                        TestButtonText = Locale.Phrases.Testing;
                        FtpTestMessageColour = Brushes.Cyan;
                        FtpTestMessage = Locale.Phrases.FtpTesting;
                        break;
                    case FtpTestStatus.Complete:
                        TestButtonEnabled = true;

                        FtpTestMessageColour = ftpTests[selectedServerId].Result == FtpTestResult.Success
                            ? Brushes.Green
                            : Brushes.Red;

                        FtpTestMessage = ftpTests[selectedServerId].Result == FtpTestResult.Success
                            ? Locale.Phrases.FtpTestSuccessful
                            : ftpTests[selectedServerId].Result == FtpTestResult.Failed
                                ? Locale.Phrases.FtpTestFailed
                                : Locale.Phrases.FtpTestCancelled;
                        break;
                }
            }
            else
            {
                FtpTestMessageVisibility = Visibility.Hidden;
                TestButtonText = Locale.Phrases.Test;
                TestButtonEnabled = FtpMode;
            }

            base.OpenFlyout();
        }

        /// <summary>
        /// Open flyout with settings
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="settings"></param>
        /// <param name="addingNew"></param>
        public void OpenFlyout(string serverId, ServerInfo settings = null, bool addingNew = false)
        {
            SetValues(serverId, settings, addingNew);

            OpenFlyout();
        }

        /// <summary>
        /// Save the server 
        /// </summary>
        public KeyValuePair<string, ServerInfo>? SaveServer()
        {
            if (ServerIsTestingFtp(selectedServerId))
            {
                MessageBox.Show(Locale.Phrases.FtpTestingInProgress, Locale.Phrases.Error);
                return null;
            }

            if (ValidateSettings())
            {
                return SaveServerSettings();
            }

            MessageBox.Show(Locale.Phrases.PleaseCorrectErrors, Locale.Phrases.Error);

            return null;
        }

        /// <summary>
        /// Validate settings
        /// </summary>
        /// <returns></returns>
        private bool ValidateSettings()
        {
            if (!ServerNameValid())
            {
                return false;
            }

            return DirectoryMode ? ValidateDirectoryModeSettings() : ValidateFtpModeSettings();
        }

        /// <summary>
        /// Validate directory settings
        /// </summary>
        /// <returns></returns>
        private bool ValidateDirectoryModeSettings()
        {
            return DirectoryBePathValid();
        }

        /// <summary>
        /// Validate FTP settings
        /// </summary>
        /// <returns></returns>
        private bool ValidateFtpModeSettings()
        {
            return FtpIpValid() && FtpPortValid() && FtpPassValid() && FtpBePathValid();
        }

        /// <summary>
        /// Save it
        /// </summary>
        private KeyValuePair<string, ServerInfo> SaveServerSettings()
        {
            SaveButtonText = Locale.Phrases.Saving;
            SaveButtonEnabled = false;

            ServerInfo server = new ServerInfo
            {
                ServerEnabled = ServerEnabled,
                RConReloadEnabled = RConReloadEnabled,
                FtpEnabled = FtpMode,
                FtpInfo = FtpMode ? GenerateFtpInfo() : null,
                Directory = DirectoryMode ? DirectoryBePath : null
            };
            
            if (selectedServerId != null && (config.Settings.Servers.ContainsKey(selectedServerId) && !addingServer))
            {
                config.UpdateServer(selectedServerId, ServerName, server);

                if (selectedServerId != ServerName)
                {
                    events.PublishOnUIThread(new ServerNameUpdateEvent()
                    {
                        OldServerName = selectedServerId,
                        NewServerName = ServerName,
                    });

                    selectedServerId = ServerName;
                }
            }
            else
            {
                config.AddServer(ServerName, server);

                events.PublishOnUIThread(new ServerAddEvent()
                {
                    NewServerName = ServerName,
                });

                selectedServerId = ServerName;
            }
            
            return new KeyValuePair<string, ServerInfo>(ServerName, server);
        }

        /// <summary>
        /// Cancel adding or editing
        /// </summary>
        public void CancelServerAddEdit()
        {
            if (ServerIsTestingFtp(selectedServerId))
            {
                // TODO Cancel connection
                MessageBox.Show(Locale.Phrases.FtpTestingInProgress, Locale.Phrases.Error);
                return;
            }

            CloseFlyout();
        }

        /// <summary>
        /// Test given FTP settings
        /// </summary>
        public void TestFtpSettings()
        {
            if (!ValidateFtpModeSettings())
            {
                MessageBox.Show(Locale.Phrases.PleaseCorrectErrors, Locale.Phrases.Error);
                return;
            }

            if (ServerIsTestingFtp(ServerName))
            {
                MessageBox.Show(Locale.Phrases.FtpTestingInProgress, Locale.Phrases.Error);
                return;
            }
            
            FtpTester ftpTest = IoC.Get<FtpTester>();

            TrackFtpServerTest(ServerName, new FtpTestInfo()
            {
                Status = FtpTestStatus.Running,
            });

            TestButtonText = Locale.Phrases.Testing;
            TestButtonEnabled = false;
            

            FtpTestMessageVisibility = Visibility.Visible;
            FtpTestMessageColour = Brushes.Cyan;
            FtpTestMessage = Locale.Phrases.FtpTesting;

            Task.Run(() => ftpTest.TestFtpSettings(selectedServerId, GenerateFtpInfo()));
        }
        
        /// <summary>
        /// Create FTP info from properties
        /// </summary>
        /// <returns></returns>
        private FtpInfo GenerateFtpInfo()
        {
            return new FtpInfo()
            {
                Ip = FtpIp,
                Port = int.Parse(FtpPort),
                User = FtpUser,
                Pass = FtpPass,
                Path = FtpBePath,
                FtpEncryptionMode = (FtpEncryptionMode)FtpEncryptionModeSelected,
                Passive = FtpPassiveEnabled,
            };
        }
        
        /// <summary>
        /// Adds/Edits server in testing list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="ftpTest"></param>
        private void TrackFtpServerTest(string key, FtpTestInfo ftpTest)
        {
            if (key == null)
            {
                return;
            }

            if (!ftpTests.ContainsKey(key))
            {
                ftpTests.Add(key, ftpTest);
            }
            else
            {
                ftpTests[key] = ftpTest;
            }
            
        }

        /// <summary>
        /// Updates server in testing list
        /// </summary>
        /// <param name="key"></param>
        /// <param name="status"></param>
        /// <param name="result"></param>
        private void UpdateFtpTestStatus(string key, FtpTestStatus status, FtpTestResult result)
        {
            if (key == null)
            {
                return;
            }

            if (!ftpTests.ContainsKey(key))
            {
                return;
            }

            ftpTests[key].Status = status;
            ftpTests[key].Result = result;
        }

        /// <summary>
        /// Check if server is test ftp
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private bool ServerIsTestingFtp(string key)
        {
            if (key == null)
            {
                return false;
            }

            if (ftpTests.ContainsKey(key))
            {
                return ftpTests[key].Status == FtpTestStatus.Running;
            }

            return false;
        }

        #region Events

        /// <summary>
        /// Update server name
        /// </summary>
        /// <param name="server"></param>
        public void Handle(ServerNameUpdateEvent server)
        {
            if (!ftpTests.ContainsKey(server.OldServerName))
            {
                return;
            }

            FtpTestInfo info = ftpTests[server.OldServerName];

            ftpTests.Remove(server.OldServerName);
            ftpTests.Add(server.NewServerName, info);
        }

        
        /// <summary>
        /// FTP test complete
        /// </summary>
        /// <param name="result"></param>
        public void Handle(FtpTestCompleteEvent result)
        {
            if (selectedServerId == result.ServerId)
            {
                FtpTestMessageVisibility = Visibility.Visible;

                FtpTestMessageColour = result.FtpTestResult == FtpTestResult.Success ? Brushes.Green : Brushes.Red;

                FtpTestMessage = result.FtpTestResult == FtpTestResult.Success
                    ? Locale.Phrases.FtpTestSuccessful
                    : result.FtpTestResult == FtpTestResult.Failed
                        ? Locale.Phrases.FtpTestFailed
                        : Locale.Phrases.FtpTestCancelled;
            }

            UpdateFtpTestStatus(result.ServerId, FtpTestStatus.Complete, result.FtpTestResult);

            _ftpTimer = new Timer(2000);
            _ftpTimer.Elapsed += FtpTimerElapsed;
            _ftpTimer.AutoReset = false;
            _ftpTimer.Enabled = true;

            _ftpTimer.Start();
        }

        private Timer _ftpTimer;
        /// <summary>
        /// _ftpTimer event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void FtpTimerElapsed(object source, ElapsedEventArgs e)
        {
            _ftpTimer?.Stop();
            _ftpTimer?.Dispose();

            TestButtonText = Locale.Phrases.Test;
            TestButtonEnabled = FtpMode;
        }

        
        /// <summary>
        /// Server saved/edited/added event
        /// </summary>
        /// <param name="save"></param>
        public void Handle(ServerSaveEvent save)
        {
            if (save.Success)
            {
                SaveButtonText = Locale.Phrases.Saved;
            }
            else
            {
                MessageBox.Show(save.Error, Locale.Phrases.Error);
            }

            _saveTimer = new Timer(2000);
            _saveTimer.Elapsed += SaveTimeElapsed;
            _saveTimer.AutoReset = false;
            _saveTimer.Enabled = true;

            _saveTimer.Start();
        }

        private Timer _saveTimer;
        /// <summary>
        /// _saveTimer event
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void SaveTimeElapsed(object source, ElapsedEventArgs e)
        {
            _saveTimer?.Stop();
            _saveTimer?.Dispose();
            
            SaveButtonText = Locale.Phrases.Save;
            SaveButtonEnabled = true;
        }
        #endregion
    }
}