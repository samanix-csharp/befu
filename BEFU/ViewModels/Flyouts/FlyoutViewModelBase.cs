﻿using System.Windows.Media;
using MahApps.Metro.Controls;

namespace Samanix.BEFU.ViewModels.Flyouts
{
    public abstract class FlyoutViewModelBase : ViewModelBase
    {
        private string _header;
        public string Header
        {
            get { return _header; }
            set
            {
                if (value.Equals(_header))
                {
                    return;
                }
                _header = value;
                NotifyOfPropertyChange(() => Header);
            }
        }
        
        private Position _position;
        public Position Position
        {
            get { return _position; }
            set
            {
                if (value.Equals(_position))
                {
                    return;
                }
                _position = value;
                NotifyOfPropertyChange(() => Position);
            }
        }
        
        private FlyoutTheme _theme;
        public FlyoutTheme Theme
        {
            get { return _theme; }
            set
            {
                if (value == _theme)
                {
                    return;
                }
                _theme = value;
                NotifyOfPropertyChange(() => Theme);
            }
        }

        public virtual void CloseFlyout()
        {
            if (IsOpen)
            {
                IsOpen = false;
            }
        }

        public virtual void OpenFlyout()
        {
            if (!IsOpen)
            {
                IsOpen = true;
            }
        }

        public void ToggleFlyout()
        {
            IsOpen = !IsOpen;
        }
    }
}