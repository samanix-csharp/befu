﻿using System;

namespace Samanix.BEFU.ViewModels
{
    public class LicenseViewModel : ViewModelBase
    {
        private string _licenseWindowTitle;
        /// <summary>
        /// Window title
        /// </summary>
        public string LicenseWindowTitle
        {
            get { return _licenseWindowTitle; }
            set
            {
                if (value.Equals(_licenseWindowTitle))
                {
                    return;
                }

                _licenseWindowTitle = value;
                NotifyOfPropertyChange(() => LicenseWindowTitle);
            }
        }

        /// <summary>
        /// Abstract method
        /// </summary>
        public override void Dispose()
        {
            throw new System.NotImplementedException();
        }

        private string _moduleName;
        /// <summary>
        /// License name
        /// </summary>
        public string ModuleName
        {
            get { return _moduleName; }
            set
            {
                _moduleName = value;
                NotifyOfPropertyChange(() => ModuleName);
            }
        }
        
        private string _licenseText;
        /// <summary>
        /// License text
        /// </summary>
        public string LicenseText
        {
            get { return _licenseText; }
            set
            {
                _licenseText = value;
                NotifyOfPropertyChange(() => LicenseText);
            }
        }
        
        /// <summary>
        /// Set values for view
        /// </summary>
        /// <param name="name"></param>
        /// <param name="module"></param>
        public void SetValues(string name, string module)
        {
            LicenseWindowTitle = string.Format(Locale.License.WindowTitleLicense, module);
            ModuleName = module;

            switch (name)
            {
                case "Caliburn":
                    LicenseText = string.Format(Locale.License.MITLicense, "2010 Blue Spire Consulting, Inc.");
                    break;
                case "MahAppsMetro":
                    LicenseText = string.Format(Locale.License.MITLicense, "2016 MahApps");
                    break;
                case "NewtonsoftJson":
                    LicenseText = string.Format(Locale.License.MITLicense, "2007 James Newton-King");
                    break;
                case "Ninject":
                    LicenseText = Locale.License.NinjectLicense;
                    break;
                case "BattleNet":
                    LicenseText = Locale.License.BattleNETLicense;
                    break;
                case "Entypo":
                    LicenseText = Locale.License.EntypoLicense;
                    break;
                case "WindowsIcons":
                    LicenseText = Locale.License.WindowsIconsLicense;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            IsOpen = true;

            base.OnActivate();
        }

        /// <summary>Called when deactivating.</summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            IsOpen = false;

            base.OnDeactivate(close);
        }

        /// <summary>
        /// Close dialog
        /// </summary>
        public void CloseWindow()
        {
            IsOpen = false;
            TryClose();
        }
    }
}