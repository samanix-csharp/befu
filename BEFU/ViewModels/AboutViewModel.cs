﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using Samanix.BEFU.Models;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        private string _aboutWindowTitle;
        /// <summary>
        /// Window title
        /// </summary>
        public string AboutWindowTitle
        {
            get { return _aboutWindowTitle; }
            set
            {
                if (value.Equals(_aboutWindowTitle))
                {
                    return;
                }

                _aboutWindowTitle = value;
                NotifyOfPropertyChange(() => AboutWindowTitle);
            }
        }

        private string _version;
        /// <summary>
        /// Store version
        /// </summary>
        public string Version
        {
            get { return _version; }
            set
            {
                _version = value;
                NotifyOfPropertyChange(() => Version);
            }
        }

        public AboutViewModel(BefuVersion version)
        {
            Version = version.VersionText;
            AboutWindowTitle = Locale.About.AboutTitle;
        }

        /// <summary>
        /// Abstract method
        /// </summary>
        public override void Dispose()
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            IsOpen = true;

            base.OnActivate();
        }

        /// <summary>Called when deactivating.</summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            IsOpen = false;

            base.OnDeactivate(close);
        }

        /// <summary>
        /// Close dialog
        /// </summary>
        public void CloseWindow()
        {
            IsOpen = false;
            TryClose();

            LicenseViewModel licenseVm = IoC.Get<ViewModelBase>("LicenseVM") as LicenseViewModel;

            if (licenseVm == null)
            {
                return;
            }

            if (licenseVm.IsOpen)
            {
                licenseVm.TryClose();
            }
        }

        /// <summary>
        /// Open a license window
        /// </summary>
        /// <param name="sender"></param>
        public void OpenLicense(object sender)
        {
            Button button = sender as Button;

            if (button == null)
            {
                Logger.Trace(Locale.Phrases.NullButtonPressed);
                return;
            }

            LicenseViewModel licenseVm = IoC.Get<ViewModelBase>("LicenseVM") as LicenseViewModel;

            if (licenseVm == null)
            {
                return;
            }

            string module = button.Content.ToString();

            if (licenseVm.IsOpen)
            {
                licenseVm.TryClose();

                if (licenseVm.ModuleName == module)
                {
                    return;
                }
            }

            licenseVm.SetValues(button.Name, module);

            IoC.Get<IWindowManager>().ShowWindow(licenseVm);
        }

    }
}