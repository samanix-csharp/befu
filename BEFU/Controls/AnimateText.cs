﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;

namespace Samanix.BEFU.Models
{
    public class AnimateText : Behavior<TextBlock>
    {
        public Duration AnimationDuration { get; set; }

        private string OldValue = null;
        private string NewValue = null;

        private DoubleAnimation AnimationOut;
        private DoubleAnimation AnimationIn;
        
        protected override void OnAttached()
        {
            base.OnAttached();

            AnimationOut = new DoubleAnimation(1, 0, AnimationDuration, FillBehavior.HoldEnd);
            AnimationIn = new DoubleAnimation(0, 1, AnimationDuration, FillBehavior.HoldEnd);

            AnimationOut.Completed += (sOut, eOut) =>
            {
                AssociatedObject.SetCurrentValue(TextBlock.TextProperty, NewValue);
                OldValue = NewValue;
                AssociatedObject.BeginAnimation(UIElement.OpacityProperty, AnimationIn);
            };

            Binding.AddTargetUpdatedHandler(AssociatedObject, Updated);
        }

        private void Updated(object sender, DataTransferEventArgs e)
        {
            string value = AssociatedObject.GetValue(TextBlock.TextProperty) as string;

            AssociatedObject.BeginAnimation(UIElement.OpacityProperty, AnimationOut);
            NewValue = value;
            
            if (OldValue == null)
            {
                OldValue = value;
            }

            AssociatedObject.SetCurrentValue(TextBlock.TextProperty, OldValue);
        }
    }
}