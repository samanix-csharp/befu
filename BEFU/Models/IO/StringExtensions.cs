﻿using System;
using System.Text;

namespace Samanix.BEFU.Models.IO
{
    public static class StringExtensions
    {
        /// <summary>
        /// Create FTP password hex
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToFtpPasswordHex(this string input)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in input)
            {
                sb.AppendFormat("%{0:X2}", (int)c);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Convert string to base64
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToBase64(this string input)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(input));
        }

        /// <summary>
        /// Convert string to base64
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string FromBase64(this string input)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(input));
        }
    }
}