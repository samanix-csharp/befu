﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Samanix.BEFU.Models.BattlEye;

namespace Samanix.BEFU.Models.IO
{
    public class LocalFilterManager : IFilterManager
    {
        /// <summary>
        /// Check if disposing underway
        /// </summary>
        public bool IsDisposing { get; private set; } = false;

        /// <summary>
        /// Check if object disposed
        /// </summary>
        public bool IsDisposed { get; private set; } = false;

        /// <summary>
        /// Gets local files
        /// </summary>
        /// <param name="server"></param>
        /// <param name="serverInfo"></param>
        /// <returns></returns>
        public BattlEyeServerFiles GetFiles(string server, ServerInfo serverInfo)
        {
            try
            {
                IList<string> allFiles = Directory.GetFiles(serverInfo.Directory).Select(Path.GetFileName).ToList();
                
                // Log files
                IList<string> logFiles = allFiles
                    .Where(l => l.EndsWith(".log", StringComparison.OrdinalIgnoreCase) && BattlEyeFiles.SupportedFiles.Contains(Path.GetFileNameWithoutExtension(l), StringComparer.OrdinalIgnoreCase))
                    .ToList();

                if (logFiles.Count == 0)
                {
                    throw new FileNotFoundException(string.Format(Locale.Befu.NoLogFilesFoundForServer, server));
                }

                // Filter files
                IList<string> filterFiles = allFiles
                    .Where(f => f.EndsWith(".txt", StringComparison.OrdinalIgnoreCase) && logFiles.Contains($"{Path.GetFileNameWithoutExtension(f)}.log", StringComparer.OrdinalIgnoreCase))
                    .ToList();

                if (filterFiles.Count == 0)
                {
                    throw new FileNotFoundException(string.Format(Locale.Befu.NoFilterFilesFoundForServer, server));
                }

                // BEServer.cfg
                IList<string> configFiles = allFiles
                    .Where(c => c.StartsWith("beserver", StringComparison.OrdinalIgnoreCase) && c.EndsWith(".cfg", StringComparison.OrdinalIgnoreCase)) // Allows for _x64, _active_XXX
                    .ToList();

                if (configFiles.Count == 0)
                {
                    Logger.Warn(Locale.Befu.NoValidBeServerCfgFound, server);
                }

                // Create BE server info
                BattlEyeServerFiles battleyeServerFiles = new BattlEyeServerFiles
                {
                    LogFileCount = logFiles.Count,
                    FilterFileCount = filterFiles.Count,
                    SourceDirectory = serverInfo.Directory,
                    FileList = allFiles,
                    FilterList = filterFiles,
                    LogList = logFiles,
                    ConfigList = configFiles,
                };

                return battleyeServerFiles;
            }
            catch (Exception e)
            {
                Logger.Error(Locale.Befu.FailedLoadingLocalFiles, server, e);
                return null;
            }
        }

        /// <summary>
        /// Unused for local
        /// </summary>
        /// <param name="server"></param>
        /// <param name="serverInfo"></param>
        /// <param name="serverFiles"></param>
        public void UploadFiles(string server, ServerInfo serverInfo, BattlEyeServerFiles serverFiles)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Unused
        /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}