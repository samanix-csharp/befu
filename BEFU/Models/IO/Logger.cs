﻿namespace Samanix.BEFU.Models.IO
{
    public enum LogLevel
    {
        None = 0,
        Trace = 1,
        Debug = 2,
        Info = 3,
        Warn = 4,
        Error = 5,
        Fatal = 6,
    }

    public static class Logger
    {
        /// <summary>
        /// Logging level
        /// </summary>
        public static LogLevel LogLevel { get; set; } = LogLevel.Info;

        public static void Log(LogLevel level, string message, params object[] parts)
        {
            if (level >= LogLevel)
            {
                LogMessage(string.Format(message, parts));
            }
        }

        public delegate void LogListenEvent(string message);

        /// <summary>
        /// Log listener event
        /// </summary>
        public static event LogListenEvent LogListener;

        /// <summary>
        /// Invoke log listener event
        /// </summary>
        /// <param name="message"></param>
        public static void LogMessage(string message)
        {
            LogListener?.Invoke(message);
        }

        #region Log Levels

        /// <summary>
        /// Trace logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parts"></param>
        public static void Trace(string message, params object[] parts)
        {
            Log(LogLevel.Trace, message, parts);
        }

        /// <summary>
        /// Debug logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parts"></param>
        public static void Debug(string message, params object[] parts)
        {
            Log(LogLevel.Debug, message, parts);
        }

        /// <summary>
        /// Info logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parts"></param>
        public static void Info(string message, params object[] parts)
        {
            Log(LogLevel.Info, message, parts);
        }

        /// <summary>
        /// Warn logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parts"></param>
        public static void Warn(string message, params object[] parts)
        {
            Log(LogLevel.Warn, message, parts);
        }

        /// <summary>
        /// Error logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parts"></param>
        public static void Error(string message, params object[] parts)
        {
            Log(LogLevel.Error, message, parts);
        }

        /// <summary>
        /// Fatal logging
        /// </summary>
        /// <param name="message"></param>
        /// <param name="parts"></param>
        public static void Fatal(string message, params object[] parts)
        {
            Log(LogLevel.Fatal, message, parts);
        }

        #endregion
    }
}