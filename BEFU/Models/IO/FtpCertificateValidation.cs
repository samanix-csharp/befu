﻿using System.Net.Security;
using Limilabs.FTP.Client;

namespace Samanix.BEFU.Models.IO
{
    public static class FtpCertificateValidation
    {
        /// <summary>
        /// Allows for self signed certs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void ValidateCertificate(object sender, ServerCertificateValidateEventArgs e)
        {
            const SslPolicyErrors ignoredErrors = SslPolicyErrors.RemoteCertificateChainErrors | SslPolicyErrors.RemoteCertificateNameMismatch;

            if ((e.SslPolicyErrors & ~ignoredErrors) == SslPolicyErrors.None)
            {
                e.IsValid = true;
                return;
            }

            e.IsValid = false;
        }
    }
}