﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using Caliburn.Micro;
using Samanix.BEFU.Events;

namespace Samanix.BEFU.Models.IO
{
    public class LogFileManager : IDisposable
    {
        /// <summary>
        /// Config instance
        /// </summary>
        private readonly Config config;

        /// <summary>
        /// Log file to write to
        /// </summary>
        private const string LogFile = "befu.log";

        /// <summary>
        /// Stores full path to log file
        /// </summary>
        private readonly string filePath;

        /// <summary>
        /// Writes to log file
        /// </summary>
        private StreamWriter writer;

        /// <summary>
        /// Lock object
        /// </summary>
        private readonly object _lock = new object();

        /// <summary>
        /// Event handler
        /// </summary>
        private readonly IEventAggregator events;

        /// <summary>
        /// Check if disposing underway
        /// </summary>
        public bool IsDisposing { get; private set; } = false;

        /// <summary>
        /// Check if object disposed
        /// </summary>
        public bool IsDisposed { get; private set; } = false;


        public LogFileManager(Config cfg, IEventAggregator eventAggregator)
        {
            config = cfg;
            events = eventAggregator;

            filePath = Path.Combine(Config.LogDirectory, LogFile);

            ArchiveOldLogFile();
        }

        #region Dispose
        /// <summary>
        /// Finalizer
        /// </summary>
        ~LogFileManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeManaged"></param>
        private void Dispose(bool freeManaged)
        {
            if (!freeManaged || IsDisposing || IsDisposed)
            {
                return;
            }

            IsDisposing = true;

            Logger.Trace(Locale.Phrases.DisposingManagedResourcesFor, GetType());

            StopLogFileManager();

            IsDisposing = false;
            IsDisposed = true;
        }
        #endregion

        /// <summary>
        /// Start log manager
        /// </summary>
        public void StartLogFileManager()
        {
            if (writer != null)
            {
                return;
            }

            lock (_lock)
            {
                writer = new StreamWriter(filePath, true, Encoding.UTF8, 4096)
                {
                    AutoFlush = true,
                };

                Logger.LogListener += Log;
            }
        }

        /// <summary>
        /// Stop log manager
        /// </summary>
        public void StopLogFileManager()
        {
            if (writer == null)
            {
                return;
            }

            lock (_lock)
            {
                try
                {
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception e)
                {
                    LogFileManagerError(e.ToString());
                }
                finally
                {
                    writer = null;
                }
            }
        }

        /// <summary>
        /// Log line handler
        /// </summary>
        /// <param name="line"></param>
        public void Log(string line)
        {
//            if (writer == null)
//            {
//                return;
//            }

            lock (_lock)
            {
                try
                {
                    string parsedLine = $"[{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}] {line}";

                    writer.WriteLine(parsedLine);

                    events.PublishOnUIThread(new LogEvent()
                    {
                        Message = parsedLine
                    });
                }
                catch (Exception e)
                {
                    LogFileManagerError(e.ToString());
                }
            }
        }

        /// <summary>
        /// Archive previous log file
        /// </summary>
        /// <param name="retry"></param>
        private void ArchiveOldLogFile(int retry = 0)
        {
            if (!File.Exists(filePath))
            {
                return;
            }

            string archiveFile = Path.Combine(Config.LogDirectory,
                $"befu_{DateTime.Now:yyyy_MM_dd_HH_mm_ss}_{retry}.log");

            if (File.Exists(archiveFile))
            {
                ArchiveOldLogFile(++retry);
            }

            File.Move(filePath, archiveFile);
        }

        /// <summary>
        /// Crude error notice
        /// </summary>
        private static void LogFileManagerError(string error)
        {
            MessageBox.Show(error, Locale.Phrases.Error);
        }
    }
}