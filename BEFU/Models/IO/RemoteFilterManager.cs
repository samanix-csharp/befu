﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using Limilabs.FTP.Client;
using Samanix.BEFU.Models.BattlEye;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.Models.IO
{
    public class RemoteFilterManager : IFilterManager, IDisposable
    {
        /// <summary>
        /// Ftp clients
        /// </summary>
        private readonly IDictionary<string, Ftp> _ftpClients = new Dictionary<string, Ftp>();

        /// <summary>
        /// Check if disposing underway
        /// </summary>
        public bool IsDisposing { get; private set; } = false;

        /// <summary>
        /// Check if object disposed
        /// </summary>
        public bool IsDisposed { get; private set; } = false;

        #region Dispose
        ~RemoteFilterManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of timer
        /// </summary>
        private void Dispose(bool freeManaged)
        {
            if (!freeManaged || IsDisposing || IsDisposed)
            {
                return;
            }

            IsDisposing = true;

            Logger.Trace(Locale.Phrases.DisposingManagedResourcesFor, GetType());

            foreach (KeyValuePair<string, Ftp> client in _ftpClients)
            {
                client.Value?.Close();
            }

            IsDisposed = true;
            IsDisposing = false;
        }
        #endregion

        /// <summary>
        /// Download files from server
        /// </summary>
        /// <param name="server"></param>
        /// <param name="serverInfo"></param>
        /// <returns></returns>
        public BattlEyeServerFiles GetFiles(string server, ServerInfo serverInfo)
        {
            Logger.Info(Locale.Befu.DownloadingBeFilesOnServer, server);
            FtpInfo ftpInfo = serverInfo.FtpInfo;

            Ftp ftpClient = GetFtpInstance(server, ftpInfo);

            try
            {
                Logger.Info(Locale.Befu.SearchDownloadFilesOnServer, server, ftpInfo.Path);

                IList<string> allFiles = ftpClient.GetList()
                    .Select(f => f.Name)
                    .ToList();

                if (allFiles.Count == 0)
                {
                    throw new FileNotFoundException(string.Format(Locale.Befu.NoFilesFoundForServer, server));
                }

                // Log files
                IList<string> logFiles = allFiles
                    .Where(
                        l =>
                            l.EndsWith(".log", StringComparison.OrdinalIgnoreCase) &&
                            BattlEyeFiles.SupportedFiles.Contains(Path.GetFileNameWithoutExtension(l),
                                StringComparer.OrdinalIgnoreCase))
                    .ToList();

                if (logFiles.Count == 0)
                {
                    throw new FileNotFoundException(string.Format(Locale.Befu.NoLogFilesFoundForServer, server));
                }

                // Filter files
                IList<string> filterFiles = allFiles
                    .Where(
                        f =>
                            f.EndsWith(".txt", StringComparison.OrdinalIgnoreCase) &&
                            logFiles.Contains($"{Path.GetFileNameWithoutExtension(f)}.log",
                                StringComparer.OrdinalIgnoreCase))
                    .ToList();

                if (filterFiles.Count == 0)
                {
                    throw new FileNotFoundException(string.Format(Locale.Befu.NoFilterFilesFoundForServer, server));
                }

                // BEServer.cfg
                IList<string> configFiles = allFiles
                    .Where(
                        c =>
                            c.StartsWith("beserver", StringComparison.OrdinalIgnoreCase) &&
                            c.EndsWith(".cfg", StringComparison.OrdinalIgnoreCase)) // Allows for _x64, _active_XXX
                    .ToList();

                if (configFiles.Count == 0)
                {
                    Logger.Warn(Locale.Befu.NoValidBeServerCfgFound, server);
                }

                IList<string> downloadList = new List<string>()
                    .Concat(logFiles)
                    .Concat(filterFiles)
                    .Concat(configFiles)
                    .ToList();

                string destination = Config.FtpDestinationDirectory(server);

                if (!Directory.Exists(destination))
                {
                    Directory.CreateDirectory(destination);
                }

                foreach (string file in downloadList)
                {
                    Logger.Info(Locale.Befu.DownloadingFile, file, server);
                    ftpClient.Download(file, Path.Combine(destination, file));
                }

                // Create BE server info
                BattlEyeServerFiles battleyeServerFiles = new BattlEyeServerFiles
                {
                    LogFileCount = logFiles.Count,
                    FilterFileCount = filterFiles.Count,
                    SourceDirectory = destination,
                    FileList = allFiles,
                    FilterList = filterFiles,
                    LogList = logFiles,
                    ConfigList = configFiles,
                };

                return battleyeServerFiles;
            }
            catch (Exception e)
            {
                Logger.Error(Locale.Befu.FailedLoadingRemoteFiles, server, e);
                return null;
            }
        }

        /// <summary>
        /// Upload files back to server, remove old logs
        /// </summary>
        /// <param name="server"></param>
        /// <param name="serverInfo"></param>
        /// <param name="serverFiles"></param>
        public void UploadFiles(string server, ServerInfo serverInfo, BattlEyeServerFiles serverFiles)
        {
            FtpInfo ftpInfo = serverInfo.FtpInfo;

            Ftp ftpClient = GetFtpInstance(server, ftpInfo);

            try
            {
                foreach (string filter in serverFiles.FilterList)
                {
                    Logger.Info(Locale.Befu.UploadingFile, filter, server);

                    ftpClient.Upload(Path.Combine(ftpInfo.Path, filter),
                        Path.Combine(serverFiles.SourceDirectory, filter));
                }

                foreach (string log in serverFiles.LogList)
                {
                    Logger.Info(Locale.Befu.DeletingFile, log, server);

                    ftpClient.DeleteFile(Path.Combine(ftpInfo.Path, log));
                }
            }
            catch (Exception e)
            {
                Logger.Error(Locale.Befu.FailedUploadingToServer, server, e);
            }
            
        }

        /// <summary>
        /// Create or get FTP instance
        /// </summary>
        /// <param name="server"></param>
        /// <param name="ftpInfo"></param>
        /// <returns></returns>
        private Ftp GetFtpInstance(string server, FtpInfo ftpInfo)
        {
            if (_ftpClients.ContainsKey(server))
            {
                if (_ftpClients[server].Connected)
                {
                    return _ftpClients[server];
                }

                _ftpClients[server].Close();
                _ftpClients.Remove(server);
            }

            Ftp client = new Ftp()
            {
                Mode = ftpInfo.Passive ? FtpMode.Passive : FtpMode.Active,
            };
            
            if (ftpInfo.FtpEncryptionMode != FtpEncryptionMode.None)
            {
                client.ServerCertificateValidate += FtpCertificateValidation.ValidateCertificate;
            }

            client.Connect(ftpInfo.Ip, ftpInfo.Port, ftpInfo.FtpEncryptionMode == FtpEncryptionMode.Implicit);

            if (ftpInfo.FtpEncryptionMode == FtpEncryptionMode.Explicit)
            {
                client.AuthTLS();
            }

            client.Login(ftpInfo.User, ftpInfo.Pass);

            client.ChangeFolder(ftpInfo.Path);

            _ftpClients.Add(server, client);

            return client;
        }
    }
}