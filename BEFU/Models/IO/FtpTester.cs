﻿using System;
using Caliburn.Micro;
using Limilabs.FTP.Client;
using Samanix.BEFU.Events;

namespace Samanix.BEFU.Models.IO
{
    public class FtpTester
    {
        /// <summary>
        /// Event aggregrator
        /// </summary>
        private readonly IEventAggregator events;

        public FtpTester(IEventAggregator eventAggregator)
        {
            events = eventAggregator;
        }

        /// <summary>
        /// Test given FTP settings
        /// </summary>
        /// <param name="server"></param>
        /// <param name="ftpInfo"></param>
        public void TestFtpSettings(string server, FtpInfo ftpInfo)
        {
            Logger.Info(Locale.Phrases.FtpTestingOnIp, ftpInfo.Ip, ftpInfo.Port);

            using (Ftp ftpClient = new Ftp())
            {
                ftpClient.Mode = ftpInfo.Passive ? FtpMode.Passive : FtpMode.Active;

                if (ftpInfo.FtpEncryptionMode != FtpEncryptionMode.None)
                {
                    ftpClient.ServerCertificateValidate += FtpCertificateValidation.ValidateCertificate;
                }

                FtpTestResult testResult = FtpTestResult.Failed;

                try
                {
                    ftpClient.Connect(ftpInfo.Ip, ftpInfo.Port, ftpInfo.FtpEncryptionMode == FtpEncryptionMode.Implicit);

                    if (ftpInfo.FtpEncryptionMode == FtpEncryptionMode.Explicit)
                    {
                        ftpClient.AuthTLS();
                    }

                    ftpClient.Login(ftpInfo.User, ftpInfo.Pass);

                    testResult = ftpClient.Connected ? FtpTestResult.Success : FtpTestResult.Failed;
                }
                catch (Exception e)
                {
                    Logger.Error(e.ToString());

                    testResult = FtpTestResult.Failed;
                }
                finally
                {
                    events.PublishOnUIThread(new FtpTestCompleteEvent()
                    {
                        Ip = ftpInfo.Ip,
                        Port = ftpInfo.Port,
                        ServerId = server,
                        FtpTestResult = testResult,
                    });

                    Logger.Info(testResult == FtpTestResult.Success ? Locale.Phrases.FtpTestSuccessfulOnIp : Locale.Phrases.FtpTestFailedOnIp, ftpInfo.Ip, ftpInfo.Port);
                }
            }
        }
    }
}