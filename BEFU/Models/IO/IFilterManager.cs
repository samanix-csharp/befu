﻿using Samanix.BEFU.Models.BattlEye;

namespace Samanix.BEFU.Models.IO
{
    public interface IFilterManager
    {
        /// <summary>
        /// Check if disposing underway
        /// </summary>
        bool IsDisposing { get; }

        /// <summary>
        /// Check if object disposed
        /// </summary>
        bool IsDisposed { get; }

        BattlEyeServerFiles GetFiles(string server, ServerInfo serverInfo);

        void UploadFiles(string server, ServerInfo serverInfo, BattlEyeServerFiles serverFiles);

        void Dispose();

    }
}