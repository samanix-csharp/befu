﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using Caliburn.Micro;
using Newtonsoft.Json;
using Samanix.BEFU.Events;
using Samanix.BEFU.Exceptions;
using Samanix.BEFU.Models.IO;
using Samanix.BEFU.Properties;

namespace Samanix.BEFU.Models
{
    public class Config
    {
        public Settings Settings { get; set; } = new Settings();

        private readonly string SettingsFile;

        public static readonly string LogDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs");
        public static readonly string FilterDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ".filters");

        private readonly IEventAggregator events;

        public Config(IEventAggregator eventAggregator)
        {
            events = eventAggregator;

            SettingsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "settings.json");
        }

        /// <summary>
        /// Load and parse settings
        /// </summary>
        public void Load()
        {
            try
            {
                Settings = JsonConvert.DeserializeObject<Settings>(LoadSettingsFile());
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                ShowErrorMessageDialog(Locale.Phrases.Error, fileNotFoundException.Message);
            }
            catch (SettingsException settingsException)
            {
                ShowErrorMessageDialog(Locale.Phrases.Error, settingsException.Message);
            }
            catch (JsonReaderException jsonReaderException)
            {
                ShowErrorMessageDialog(Locale.Phrases.Error, $"{Locale.Phrases.ErrorLoadingSettings}\n\n{jsonReaderException.Message}");
            }
            catch (JsonSerializationException jsonSerializationException)
            {
                if (jsonSerializationException.InnerException is SettingsException)
                {
                    ShowErrorMessageDialog(Locale.Phrases.Error, jsonSerializationException.InnerException.Message);
                }
                else
                {
                    ShowErrorMessageDialog(Locale.Phrases.Error, jsonSerializationException.Message);
                }
            }
        }

        /// <summary>
        /// Load settings.json file contents
        /// </summary>
        /// <returns></returns>
        private string LoadSettingsFile()
        {
            if (!File.Exists(SettingsFile))
            {
                Logger.Warn(Locale.Phrases.SettingsNotFoundCreating);

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);

                using (JsonWriter jsonWriter = new JsonTextWriter(sw))
                {
                    jsonWriter.Formatting = Formatting.Indented;

                    jsonWriter.WriteStartObject();

                    jsonWriter.WritePropertyName("servers");
                    jsonWriter.WriteStartObject();
                    jsonWriter.WriteEndObject();

                    jsonWriter.WriteEndObject();
                }

                File.WriteAllText(SettingsFile, sb.ToString());
            }

            return File.ReadAllText(SettingsFile);
        }

        /// <summary>
        /// Show dialog box with errors
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        private static void ShowErrorMessageDialog(string title, string message)
        {
            MessageBox.Show(message, title);
        }

        /// <summary>
        /// Save config/settings
        /// </summary>
        public void SaveSettings(ServerSaveAction action)
        {
            bool success = true;
            Exception error = null;

            try
            {
                File.WriteAllText(SettingsFile,
                    JsonConvert.SerializeObject(Settings, Formatting.Indented, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }));
            }
            catch (Exception e)
            {
                Logger.Fatal(e.ToString());
                success = false;
                error = e;
            }
            finally
            {
                Logger.Info(success ? Locale.Phrases.SavedSettings : Locale.Phrases.SavedSettingsError);

                events.PublishOnUIThread(new ServerSaveEvent()
                {
                    Success = success,
                    Action = action,
                    Error = error?.Message,
                });
            }
        }

        /// <summary>
        /// Load single server
        /// </summary>
        /// <param name="serverId"></param>
        /// <returns></returns>
        public ServerInfo GetServerSettings(string serverId)
        {
            return Settings.Servers.ContainsKey(serverId) ? Settings.Servers[serverId] : null;
        }

        /// <summary>
        /// Add Server
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="server"></param>
        /// <param name="save"></param>
        public void AddServer(string serverId, ServerInfo server, bool save = true)
        {
            Settings.Servers.Add(serverId, server);

            if (save)
            {
                SaveSettings(ServerSaveAction.Added);
            }
        }

        /// <summary>
        /// Update existing server
        /// </summary>
        /// <param name="oldServerId"></param>
        /// <param name="newServerId"></param>
        /// <param name="server"></param>
        /// <param name="save"></param>
        public void UpdateServer(string oldServerId, string newServerId, ServerInfo server, bool save = true)
        {
            if (oldServerId != newServerId)
            {
                DeleteServer(oldServerId, false);
            }

            Settings.Servers[newServerId] = server;

            if (save)
            {
                SaveSettings(ServerSaveAction.Updated);
            }
        }

        /// <summary>
        /// Delete server
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="save"></param>
        public void DeleteServer(string serverId, bool save = true)
        {
            if (!Settings.Servers.ContainsKey(serverId))
            {
                return;
            }

            Settings.Servers.Remove(serverId);

            if (save)
            {
                SaveSettings(ServerSaveAction.Deleted);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        public static string FtpDestinationDirectory(string server)
        {
            return Path.Combine(FilterDirectory, server);
        }
    }
}