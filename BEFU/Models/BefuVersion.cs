﻿using System.Linq;
using System.Reflection;

namespace Samanix.BEFU.Models
{
    public class BefuVersion
    {
        private readonly string _version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        private string _normalisedVersion = null;

        public string Version => Normalise(_version);
        public string VersionText => $"{Locale.Phrases.Version} {Version}";


        private string Normalise(string version)
        {
            if (_normalisedVersion != null)
            {
                return _normalisedVersion;
            }

            string[] parts = version.Split('.');

            parts = parts.Take(parts.Count() - 1).ToArray();

            _normalisedVersion = string.Join(".", parts);

            return _normalisedVersion;
        }
    }
}