﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Caliburn.Micro;
using Samanix.BEFU.Events;
using Samanix.BEFU.Exceptions;
using Samanix.BEFU.Models.BattlEye;
using Samanix.BEFU.Models.BattlEye.Filter;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.Models
{
    public class FilterUpdater : IDisposable
    {
        private Config config;
        private readonly IDictionary<string, ServerInfo> servers;
        private readonly IEventAggregator events;
        
        private readonly IDictionary<string, BattlEyeServerFiles> battleyeServerFiles = new Dictionary<string, BattlEyeServerFiles>();

        /// <summary>
        /// Check if disposing underway
        /// </summary>
        public bool IsDisposing { get; private set; } = false;

        /// <summary>
        /// Check if object disposed
        /// </summary>
        public bool IsDisposed { get; private set; } = false;

        public FilterUpdater(Config cfg, IEventAggregator eventAggregator)
        {
            config = cfg;
            servers = config.Settings.Servers;
            events = eventAggregator;

            events.Subscribe(this);
        }

        ~FilterUpdater()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of timer
        /// </summary>
        private void Dispose(bool freeManaged)
        {
            if (!freeManaged || IsDisposing || IsDisposed)
            {
                return;
            }

            IsDisposing = true;

            Logger.Trace(Locale.Phrases.DisposingManagedResourcesFor, GetType());
            battleyeServerFiles.Clear();

            IsDisposing = false;
            IsDisposed = true;
        }

        /// <summary>
        /// Run
        /// </summary>
        public void Run()
        {
            Task.Run(() => ExecuteBefu());
        }

        /// <summary>
        /// Execute 
        /// </summary>
        private void ExecuteBefu()
        {
            bool success = false;

            try
            {
                LoadFiles();

                ReadFiles();

                ParseFiles();
#if !DEBUG
                UploadCleanupFiles();

                ReloadFilters();
#endif
                IoC.Get<IFilterManager>("RemoteFilterManager").Dispose();

                success = true;
            }
            catch (Exception e)
            {
                // TODO Improve so execution can continue?
                Logger.Error(e.ToString());
                success = false;
            }
            finally
            {
                events.PublishOnUIThread(new BefuRunComplete()
                {
                    Success = success,
                });
            }
        }

        /// <summary>
        /// Load scripts, logs, and beserver.cfgs
        /// </summary>
        private void LoadFiles()
        {
            IDictionary<string, ServerInfo> enabledServers = servers.Where(server => server.Value.ServerEnabled).ToDictionary(server => server.Key, server => server.Value);

            if (enabledServers.Count == 0)
            {
                throw new NoEnabledServersException(Locale.Befu.NoEnabledServersFound);
            }

            foreach (KeyValuePair<string, ServerInfo> server in enabledServers)
            {
                if (battleyeServerFiles.ContainsKey(server.Key))
                {
                    continue;
                }

                Logger.Info(Locale.Befu.LoadingFilesForServer, server.Key);


                BattlEyeServerFiles beInfo =
                    IoC.Get<IFilterManager>(server.Value.FtpEnabled ? "RemoteFilterManager" : "LocalFilterManager")
                        .GetFiles(server.Key, server.Value);

                if (beInfo == null)
                {
                    Logger.Error(Locale.Befu.NoFilesFoundToParseFor, server.Key);
                    continue;
                }

                battleyeServerFiles.Add(server.Key, beInfo);
            }
        }

        /// <summary>
        /// Read loaded files
        /// </summary>
        private void ReadFiles()
        {
            if (battleyeServerFiles.Count == 0)
            {
                throw new NoBeFilesLoadedException(Locale.Befu.NoBEFilesLoaded);
            }

            foreach (KeyValuePair<string, BattlEyeServerFiles> be in battleyeServerFiles)
            {
                if (be.Value == null)
                {
                    Logger.Error(Locale.Befu.FileInfoNullSkipping, be.Key);
                    continue;
                }

                if (be.Value.ConfigList.Count > 0)
                {
                    // TODO Try all? Test rcon each time? Might cause longer lookup if server off though
                    battleyeServerFiles[be.Key].BeServerCfg = ParseBeServerCfg(be.Key, Path.Combine(be.Value.SourceDirectory, be.Value.ConfigList[0]));
                }

                // The filter list contains supported filters that exist with a log file as well
                foreach (string file in be.Value.FilterList)
                {
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file);

                    if (fileNameWithoutExtension == null)
                    {
                        Logger.Error(Locale.Befu.NullValueWhenLoadingFiles, be.Key);
                        continue;
                    }

                    string filterType = fileNameWithoutExtension.ToLower();

                    IBattlEyeFilter filterParser;

                    switch (filterType)
                    {
                        case "scripts":
                            filterParser = new Scripts(be.Value.SourceDirectory);
                            break;
                        case "addweaponcargo":
                            filterParser = new AddWeaponCargo(be.Value.SourceDirectory);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(Locale.Befu.CannotLoadInvalidFilter, filterType);
                    }
                    

                    battleyeServerFiles[be.Key].LoadedFiles.Add(filterType, filterParser);
                }
            }
        }

        /// <summary>
        /// Parse read files
        /// </summary>
        private void ParseFiles()
        {
            if (battleyeServerFiles.Count == 0)
            {
                throw new NoBeFilesLoadedException(Locale.Befu.NoBEFilesLoaded);
            }

            foreach (KeyValuePair<string, BattlEyeServerFiles> be in battleyeServerFiles)
            {
                foreach (KeyValuePair<string, IBattlEyeFilter> filter in be.Value.LoadedFiles)
                {
                    Logger.Debug(Locale.Befu.ReadingFilesFor, filter.Key);
                    filter.Value.ReadFilter();
                    filter.Value.ReadLog();
                    filter.Value.ParseLog();
                    filter.Value.WriteToFilter();
                }
            }
        }

        /// <summary>
        /// Reupload parsed filters to remote servers, remove downloaded and local ones
        /// </summary>
        private void UploadCleanupFiles()
        {
            IDictionary<string, BattlEyeServerFiles> beServerFiles =
                battleyeServerFiles.Where(s => servers.ContainsKey(s.Key) && servers[s.Key].ServerEnabled)
                    .ToDictionary(s => s.Key, s => s.Value);

            if (beServerFiles.Count == 0)
            {
                Logger.Info(Locale.Befu.NoFilesToUploadOrCleanup);
                return;
            }

            foreach (KeyValuePair<string, BattlEyeServerFiles> server in beServerFiles)
            {
                if (servers[server.Key].FtpEnabled)
                {
                    Logger.Info(Locale.Befu.UploadingNewExceptions, server.Key);

                    if (!Directory.Exists(server.Value.SourceDirectory))
                    {
                        Logger.Error(Locale.Befu.DirectoryNotFoundAt, server.Value.SourceDirectory);
                        continue;
                    }

                    IoC.Get<IFilterManager>("RemoteFilterManager")
                        .UploadFiles(server.Key, servers[server.Key], server.Value);

                    Directory.Delete(battleyeServerFiles[server.Key].SourceDirectory, true);
                }
                else
                {
                    foreach (string log in server.Value.LogList)
                    {
                        string pathToLog = Path.Combine(server.Value.SourceDirectory, log);

                        if (File.Exists(pathToLog))
                        {
                            Logger.Info(Locale.Befu.DeletingFile, log, server.Key);
                            File.Delete(pathToLog);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Reload filters via rcon
        /// </summary>
        private void ReloadFilters()
        {
            IDictionary<string, BattlEyeServerFiles> rconServers =
                battleyeServerFiles.Where(
                        s => servers.ContainsKey(s.Key) && servers[s.Key].ServerEnabled && servers[s.Key].RConReloadEnabled)
                    .ToDictionary(s => s.Key, s => s.Value);

            if (rconServers.Count == 0)
            {
                Logger.Info(Locale.Befu.NoServersToReloadFiltersOn);
                return;
            }

            foreach (KeyValuePair<string, BattlEyeServerFiles> server in rconServers)
            {
                if (server.Value.BeServerCfg == null)
                {
                    // TODO Store file name in parse and return a bool to determine if valid, return attempted parsed file name to logger
                    Logger.Error(Locale.Befu.NoValidBEServerConfigParsed, server.Key);
                    continue;
                }

                RConClient rcon = new RConClient(server.Key, server.Value.BeServerCfg.RConIp, server.Value.BeServerCfg.RConPort, server.Value.BeServerCfg.RConPass);

                rcon.ReloadFilters();
            }
        }

        /// <summary>
        /// Parse values out of BE Server cfg
        /// </summary>
        /// <param name="server"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        private BattlEyeServerCfg ParseBeServerCfg(string server, string file)
        {
            Logger.Info(Locale.Befu.ParsingBeServerCfg, server);

            if (!File.Exists(file))
            {
                Logger.Error(Locale.Befu.NoValidBeServerCfgFound, server);
                return null;
            }

            string[] lines = File.ReadAllLines(file);

            BattlEyeServerCfg beServerCfg = new BattlEyeServerCfg();

            foreach (string line in lines)
            {
                if (line.StartsWith("RConPassword", StringComparison.OrdinalIgnoreCase))
                {
                    beServerCfg.RConPass = Regex.Replace(line, "RConPassword", "", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase).Trim();
                    Logger.Debug(Locale.Befu.ParsedBeServerItem, "RConPassword", server, beServerCfg.RConPass);
                }

                if (line.StartsWith("RConIP", StringComparison.OrdinalIgnoreCase))
                {
                    beServerCfg.RConIp = Regex.Replace(line, "RConIP", "", RegexOptions.IgnoreCase).Trim();
                    Logger.Debug(Locale.Befu.ParsedBeServerItem, "RConIP", server, beServerCfg.RConIp);
                }

                if (line.StartsWith("RConPort", StringComparison.OrdinalIgnoreCase))
                {
                    string port = Regex.Replace(line, "RConPort", "", RegexOptions.IgnoreCase).Trim();
                    int parsedPort;

                    if (int.TryParse(port, out parsedPort))
                    {
                        beServerCfg.RConPort = parsedPort;
                        Logger.Debug(Locale.Befu.ParsedBeServerItem, "RConPort", server, beServerCfg.RConPort);
                    }
                }
            }

            if (!servers.ContainsKey(server))
            {
                return null;
            }

            if (beServerCfg.RConPass == null)
            {
                Logger.Error(Locale.Befu.RConPasswordNotFoundDisablingReload);
                return null;
            }

            if (beServerCfg.RConIp == null)
            {
                beServerCfg.RConIp = servers[server].FtpEnabled ? servers[server].FtpInfo.Ip : "127.0.0.1";
                Logger.Warn(Locale.Befu.NoRConIpFoundDefaultingTo, beServerCfg.RConIp, server);
            }

            return beServerCfg;
        }
        
    }
}