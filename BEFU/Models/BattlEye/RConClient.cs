﻿using System;
using System.Net;
using BattleNET;
using Samanix.BEFU.Exceptions;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.Models.BattlEye
{
    public class RConClient
    {
        /// <summary>
        /// Server name
        /// </summary>
        private readonly string _server;

        /// <summary>
        /// Host to connect to
        /// </summary>
        private readonly string _host;

        /// <summary>
        /// RCon port
        /// </summary>
        private readonly int _port;

        /// <summary>
        /// RCon password
        /// </summary>
        private readonly string _password;
        
        public RConClient(string server, string host, int port, string password)
        {
            _server = server;
            _host = host;
            _port = port;
            _password = password;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReloadFilters()
        {
            BattlEyeClient beClient = null;

            try
            {
                beClient = new BattlEyeClient(new BattlEyeLoginCredentials()
                {
                    Host = Dns.GetHostAddresses(_host)[0],
                    Port = _port,
                    Password = _password,
                });

                beClient.BattlEyeMessageReceived += BattlEyeMessageReceived;
                beClient.BattlEyeConnected += BattlEyeConnected;
                beClient.BattlEyeDisconnected += BattlEyeDisconnected;
                beClient.ReconnectOnPacketLoss = true;

                BattlEyeConnectionResult connectionResult = beClient.Connect();

                if (connectionResult != BattlEyeConnectionResult.Success)
                {
                    throw new RConException(connectionResult == BattlEyeConnectionResult.ConnectionFailed ? Locale.Rcon.ConnectionFailed : Locale.Rcon.InvalidLogin);
                }

                Logger.Info(Locale.Rcon.ReloadingFilters, _server);
                beClient.SendCommand("loadScripts");
                beClient.SendCommand("loadEvents");

                while (beClient.CommandQueue > 0)
                {
                    // Wait for server to get packets
                }
            }
            catch (Exception e)
            {
                Logger.Error(Locale.Rcon.RConReloadFailed, _server, e.ToString());
            }
            finally
            {
                beClient?.Disconnect();
            }
        }

        /// <summary>
        /// BE Disconnected
        /// </summary>
        /// <param name="args"></param>
        private void BattlEyeDisconnected(BattlEyeDisconnectEventArgs args)
        {
            string message;

            switch (args.DisconnectionType)
            {
                case BattlEyeDisconnectionType.Manual:
                    message = Locale.Rcon.DisconnectedFrom;
                    break;
                case BattlEyeDisconnectionType.ConnectionLost:
                    message = Locale.Rcon.LostConnectionTo;
                    break;
                case BattlEyeDisconnectionType.SocketException:
                    message = Locale.Rcon.SocketException;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            //Console.WriteLine(message, _server);
            Logger.Info(message, _server);
        }

        /// <summary>
        /// BE Connected
        /// </summary>
        /// <param name="args"></param>
        private void BattlEyeConnected(BattlEyeConnectEventArgs args)
        {
            string message;

            switch (args.ConnectionResult)
            {
                case BattlEyeConnectionResult.Success:
                    message = Locale.Rcon.ConnectedTo;
                    break;
                case BattlEyeConnectionResult.ConnectionFailed:
                    message = Locale.Rcon.ConnectionFailed;
                    break;
                case BattlEyeConnectionResult.InvalidLogin:
                    message = Locale.Rcon.InvalidLogin;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            //Console.WriteLine(message, _server);
            Logger.Info(message, _server);
        }

        /// <summary>
        /// BE message recieved
        /// </summary>
        /// <param name="args"></param>
        private void BattlEyeMessageReceived(BattlEyeMessageEventArgs args)
        {
            //Console.WriteLine(Locale.Rcon.RConMessage, _server, args.Message);
            //Logger.Info(Locale.Rcon.RConMessage, _server, args.Message);
        }
    }
}