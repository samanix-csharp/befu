﻿using System;
using System.Collections.Generic;

namespace Samanix.BEFU.Models.BattlEye
{
    public static class BattlEyeFiles
    {
        /*
        /// <summary>
        /// Ignored file list
        /// </summary>
        public static readonly IList<string> Ignored = new List<string>()
        {
            "BEServer.dll",
            "BEClient.dll",
            "eula.txt",
        };
        */

        /// <summary>
        /// List of supported files
        /// </summary>
        public static readonly IList<string> SupportedFiles = new List<string>()
        {
            "scripts",
            "addweaponcargo",
        };

        /*
        /// <summary>
        /// Other non filter/log file list
        /// </summary>
        public static readonly IList<string> Files = new List<string>()
        {
            "beserver"
        };
        */


    }
}