﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.Models.BattlEye.Filter
{
    public abstract class BattlEyeFilterBase : IBattlEyeFilter
    {
        #region Properties
        /// <summary>
        /// Filter type
        /// </summary>
        protected string FilterName;

        /// <summary>
        /// Stores log file
        /// </summary>
        public IList<string> RawLogFile { get; protected set; }

        /// <summary>
        /// Log lines after parsing
        /// </summary>
        public IDictionary<int, IList<string>> ParsedLogFile { get; } = new Dictionary<int, IList<string>>();

        /// <summary>
        /// Stores filter file
        /// </summary>
        public IList<string> FilterFile { get; protected set; }
        
        /// <summary>
        /// Full path to filter file
        /// </summary>
        protected string FilterFilePath { get; set; }

        /// <summary>
        /// Full path to log file
        /// </summary>
        protected string LogFilePath { get; set; }

        /// <summary>
        /// Directory for files
        /// </summary>
        protected string FileDirectory { get; set; }
        
        /// <summary>
        /// Regex for parsing log lines
        /// </summary>
        protected Regex LogRegex { get; set; }

        /// <summary>
        /// Regex for determining if is line if start of new log entry
        /// </summary>
        protected Regex NewLogEntryRegex { get; set; }
        #endregion
        
        protected BattlEyeFilterBase(string path)
        {
            FilterName = GetType().Name.ToLower();
            
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(string.Format(Locale.Befu.DirectoryNotFoundAt, path));
            }

            FileDirectory = path;

            FilterFilePath = Path.Combine(FileDirectory, $"{FilterName}.txt");
            LogFilePath = Path.Combine(FileDirectory, $"{FilterName}.log");

            if (!File.Exists(LogFilePath))
            {
                throw new FileNotFoundException(string.Format(Locale.Befu.FileNotFoundAt, LogFilePath));
            }

            if (!File.Exists(FilterFilePath))
            {
                throw new FileNotFoundException(string.Format(Locale.Befu.FileNotFoundAt, FilterFilePath));
            }
            
            CreateRegexes();
        }

        /// <summary>
        /// Create regex
        /// </summary>
        /// <returns></returns>
        protected void CreateRegexes()
        {
            string[] logRegex =
            {
                @"^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\.(0[1-9]|1[0-2])\.([0-9]{4})", // Date
                @"(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]):", // Time
                @"(?:.*)", // Player name
                @"(\((?:[0-9]{1,3}\.){3}[0-9]{1,3}:[0-9]{4,5}\))", // IP:Port
                @"([a-f0-9]{32}) -", // GUID
                @"#(?<KickLine>(\d+))", // Kick Line 
                @"(?<Kick>.*)", // Start of kick message
            };

            LogRegex = new Regex(string.Join(" ", logRegex));

            string[] newEntryRegex =
            {
                @"^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\.(0[1-9]|1[0-2])\.([0-9]{4})", // Date
                @"(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]):", // Time
            };

            NewLogEntryRegex = new Regex(string.Join(" ", newEntryRegex));
        }

        /// <summary>
        /// Read log file
        /// 
        /// Exceptions are handled further up callstack
        /// </summary>
        public void ReadLog()
        {
            RawLogFile = File.ReadAllLines(LogFilePath).ToList();
        }

        /// <summary>
        /// Read filter file
        /// 
        /// Exceptions are handled further up callstack
        /// </summary>
        public void ReadFilter()
        {
            FilterFile = File.ReadAllLines(FilterFilePath).ToList();
        }

        /// <summary>
        /// Parse log list 
        /// TODO Trace/Debug logging
        /// </summary>
        public virtual void ParseLog()
        {
            IList<string> holdingList = new List<string>();

            string last = RawLogFile.Last();
            int currentLine = -1;

            foreach (string line in RawLogFile)
            {
                if (NewLogEntryRegex.Match(line).Success)
                {
                    Match m = LogRegex.Match(line);
                    
                    // Got a new line, compile previous
                    AddNewLine(currentLine, holdingList);

                    // Reassign current line after compiling previous
                    currentLine = int.Parse(m.Groups["KickLine"].Value);

                    // Clear it for next
                    holdingList.Clear();
                    
                    holdingList.Add(ParseLineParts(m.Groups["Kick"].Value));
                }
                else
                {
                    if (currentLine == -1)
                    {
                        continue;
                    }

                    holdingList[holdingList.Count - 1] += ParseLineParts(line, true);
                }

                // Last line won't match a new entry, so handle here
                if (line.Equals(last))
                {
                    AddNewLine(currentLine, holdingList);
                }
            }
        }

        /// <summary>
        /// Add line to the dictionary
        /// </summary>
        /// <param name="kickNumber"></param>
        /// <param name="filter"></param>
        protected virtual void AddNewLine(int kickNumber, IList<string> filter)
        {
            if (filter.Count == 0)
            {
                return;
            }

            // The actual human read line number is kick # +2
            // We add 1 as the lists starting index is 0
            int indexedLineNumber = kickNumber + 1;

            if (!ParsedLogFile.ContainsKey(indexedLineNumber))
            {
                ParsedLogFile.Add(indexedLineNumber, new List<string>());
            }
            
            string line = string.Join("", filter.ToArray());
            string finalLine = $"!=\"{line.Substring(2, line.Length - 4)}\"";

            if (ParsedLogFile[indexedLineNumber].Contains(finalLine))
            {
                Logger.Trace(Locale.Befu.FilterExceptionExistsInList);
                return;
            }
            
            ParsedLogFile[indexedLineNumber].Add(finalLine);
        }

        /// <summary>
        /// Add line to dictionary, parsing line to int first
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <param name="filter"></param>
        protected virtual void AddNewLine(string lineNumber, IList<string> filter)
        {
            AddNewLine(int.Parse(lineNumber), filter);
        }

        /// <summary>
        /// Write parsed logs to filter
        /// </summary>
        public virtual void WriteToFilter()
        {
            foreach (KeyValuePair<int, IList<string>> adds in ParsedLogFile)
            {
                if (adds.Value.Count < 1)
                {
                    continue;
                }

                string compiledFilters = string.Join(" ", adds.Value.ToArray());
                
                if (FilterFile.ElementAtOrDefault(adds.Key) != null)
                {
                    FilterFile[adds.Key] = $"{FilterFile[adds.Key]} {compiledFilters}";;
                }
                else
                {
                    Logger.Error(Locale.Befu.KickLineNotFound, adds.Key);
                }
            }

            Logger.Info(Locale.Befu.UpdatingFilterWithNewExceptions, FilterName);

            File.WriteAllLines(FilterFilePath, FilterFile.ToArray());
        }

        /// <summary>
        /// Parses special parts of a string to be compatable with BE filters
        /// </summary>
        /// <param name="line"></param>
        /// <param name="isNewLine"></param>
        /// <returns></returns>
        protected virtual string ParseLineParts(string line, bool isNewLine = false)
        {
            return (isNewLine ? "\\n" : "") + line.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }
    }
}