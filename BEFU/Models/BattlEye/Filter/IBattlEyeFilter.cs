﻿using System.Collections.Generic;

namespace Samanix.BEFU.Models.BattlEye.Filter
{
    public interface IBattlEyeFilter
    {
        /// <summary>
        /// Stores log file
        /// </summary>
        IList<string> RawLogFile { get; }

        /// <summary>
        /// Log lines after parsing
        /// </summary>
        IDictionary<int, IList<string>> ParsedLogFile { get; }

        /// <summary>
        /// Stores filter file
        /// </summary>
        IList<string> FilterFile { get; }
        
        /// <summary>
        /// Read log file
        /// </summary>
        void ReadLog();

        /// <summary>
        /// Read filter file
        /// </summary>
        void ReadFilter();

        /// <summary>
        /// Parse log list 
        /// </summary>
        void ParseLog();

        /// <summary>
        /// Write parsed logs to filter
        /// </summary>
        void WriteToFilter();
    }
}