﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.Models.BattlEye.Filter
{
    public class AddWeaponCargo : BattlEyeFilterBase
    {
        public AddWeaponCargo(string path) : base(path)
        {
        }
        
        /// <summary>
        /// Add line to the dictionary
        /// </summary>
        /// <param name="kickNumber"></param>
        /// <param name="filter"></param>
        protected override void AddNewLine(int kickNumber, IList<string> filter)
        {
            if (filter.Count == 0)
            {
                return;
            }

            // The actual human read line number is kick # +2
            // We add 1 as the lists starting index is 0
            int indexedLineNumber = kickNumber + 1;

            if (!ParsedLogFile.ContainsKey(indexedLineNumber))
            {
                ParsedLogFile.Add(indexedLineNumber, new List<string>());
            }
            
            string line = string.Join("", filter.ToArray());
            
            Regex regex = new Regex("\"(?<item>.*?)\"");
            Match match = regex.Match(line);

            if (!match.Success)
            {
                Logger.Error(Locale.Befu.InvalidFilterString, $"{FilterName}.log");
                return;
            }

            string finalLine = $"!={match.Groups["item"].Value}";

            if (ParsedLogFile[indexedLineNumber].Contains(finalLine))
            {
                Logger.Trace(Locale.Befu.FilterExceptionExistsInList);
                return;
            }

            ParsedLogFile[indexedLineNumber].Add(finalLine);
        }

        /// <summary>
        /// Parses special parts of a string to be compatable with BE filters
        /// </summary>
        /// <param name="line"></param>
        /// <param name="isNewLine"></param>
        /// <returns></returns>
        protected override string ParseLineParts(string line, bool isNewLine = false)
        {
            return line;
        }
    }
}