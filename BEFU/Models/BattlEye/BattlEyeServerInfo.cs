﻿using System.Collections.Generic;
using Samanix.BEFU.Models.BattlEye.Filter;

namespace Samanix.BEFU.Models.BattlEye
{
    public class BattlEyeServerFiles
    {
        public IList<string> FileList { get; set; }

        public IList<string> FilterList { get; set; }

        public IList<string> LogList { get; set; }

        public IList<string> ConfigList { get; set; }

        public int LogFileCount { get; set; }

        public int FilterFileCount { get; set; }

        public string SourceDirectory { get; set; }
        
        /// <summary>
        /// Stores logs/filters once read
        /// </summary>
        public IDictionary<string, IBattlEyeFilter> LoadedFiles = new Dictionary<string, IBattlEyeFilter>();

        public BattlEyeServerCfg BeServerCfg { get; set; }
    }
    
    public class BattlEyeServerCfg
    {
        public string FileName { get; set; } = null;

        public string RConIp { get; set; } = null;

        public int RConPort { get; set; } = 2305;

        public string RConPass { get; set; } = null;
    }
}