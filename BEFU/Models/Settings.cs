﻿using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Samanix.BEFU.Models.IO;

namespace Samanix.BEFU.Models
{
    public class Settings
    {
        [JsonProperty(PropertyName = "servers")]
        public SortedDictionary<string, ServerInfo> Servers { get; set; }
    }

    public class ServerInfo
    {
        [JsonProperty(PropertyName = "enabled")]
        public bool ServerEnabled { get; set; } = true;

        [JsonProperty(PropertyName = "rconreload")]
        public bool RConReloadEnabled { get; set; } = true;

        [JsonProperty(PropertyName = "ftp")]
        public bool FtpEnabled { get; set; } = true;

        [JsonProperty(PropertyName = "ftpinfo", Required = Required.Default)]
        public FtpInfo FtpInfo { get; set; } = null;

        [JsonProperty(PropertyName = "directory", Required = Required.Default)]
        public string Directory { get; set; } = null;
    }

    public class FtpInfo
    {
        [JsonIgnore] private string _ip = "127.0.0.1";

        // TODO Dns.GetHostAddresses() will return IP if IP is given
        [JsonProperty(PropertyName = "ip")]
        public string Ip
        {
            get { return _ip; }
            set
            {
                _ip = value;

                IPAddress parsedIp;

                if (IPAddress.TryParse(_ip, out parsedIp))
                {
                    IsHostname = false;
                    ParsedIp = parsedIp;
                }
                else // Hostname
                {
                    IsHostname = true;
                    Hostname = value;
                }
            }
        }

        [JsonProperty(PropertyName = "port")]
        public int Port { get; set; } = 21;

        [JsonProperty(PropertyName = "user")]
        public string User { get; set; } = "";

        /// <summary>
        /// Do not set, set via Pass instead
        /// </summary>
        [JsonProperty(PropertyName = "pass")]
        public string EncodedPass { get; private set; }

        /// <summary>
        /// TODO Improve so password is properly encrypted
        /// </summary>
        [JsonIgnore]
        public string Pass
        {
            get
            {
                try
                {
                    return EncodedPass.FromBase64();
                }
                catch
                {
                    // Might be a user input in settings.json, just return it
                    return EncodedPass;
                }
            }
            set { EncodedPass = value.ToBase64(); }
        }

        [JsonProperty(PropertyName = "path")]
        public string Path { get; set; } = "/";

        [JsonProperty(PropertyName = "ftps", Required = Required.Default)]
        public FtpEncryptionMode FtpEncryptionMode { get; set; } = FtpEncryptionMode.None;

        [JsonProperty(PropertyName = "passive", Required = Required.Default)]
        public bool Passive { get; set; } = false;

        [JsonIgnore] public bool IsHostname = false;

        [JsonIgnore]
        public IPAddress ParsedIp { get; set; }

        [JsonIgnore]
        public string Hostname { get; set; }
    }

    public enum FtpEncryptionMode
    {
        None,
        Implicit,
        Explicit,
    }
}