﻿using Samanix.BEFU.Models;

namespace Samanix.BEFU.Events
{
    public enum FtpTestStatus
    {
        NotRunning,
        Running,
        Complete,
    }

    public enum FtpTestResult
    {
        Cancelled,
        Failed,
        Success,
    }

    public class FtpTestInfo
    {
        public FtpTestStatus Status = FtpTestStatus.NotRunning;
        
        public FtpTestResult Result = FtpTestResult.Failed;
    }

    public class FtpTestCompleteEvent
    {
        public FtpTestResult FtpTestResult { get; set; } = FtpTestResult.Failed;

        public string ServerId { get; set; }

        public string Ip { get; set; }
        
        public int Port { get; set; }
    }
    
}