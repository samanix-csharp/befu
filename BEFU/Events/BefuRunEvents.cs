﻿using System;
using System.Collections.Generic;

namespace Samanix.BEFU.Events
{
    public class BefuRunComplete
    {
        public bool Success = true;

        public string StatusMessage = string.Empty;
    }

    public class BeRconEvent
    {
        public bool Success = false;

        public string StatusMessage = string.Empty;
    }
}