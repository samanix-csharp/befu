﻿namespace Samanix.BEFU.Events
{
    public class ServerNameUpdateEvent
    {
        public string OldServerName { get; set; }
        
        public string NewServerName { get; set; }
    }

    public class ServerAddEvent
    {
        public string NewServerName { get; set; }
    }

    public enum ServerSaveAction
    {
        Added,
        Updated,
        Deleted,
    }

    public class ServerSaveEvent
    {
        public bool Success { get; set; }

        public ServerSaveAction Action { get; set; }

        public string Error { get; set; }
    }
}