﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Samanix.BEFU
{
    public partial class App
    {
        public App()
        {
            InitializeComponent();
        }

        public void UnhandledExceptionHandle(object sender, DispatcherUnhandledExceptionEventArgs eventArgs)
        {
            MessageBox.Show(eventArgs.Exception.InnerException?.Message ?? eventArgs.Exception.Message, Locale.Phrases.Error);
        }
    }
}